#-keep class ca.mudar.huegoallight.** { *; }
-keep, includedescriptorclasses class com.philips.lighting.** { *; }

-dontobfuscate

-keep class ca.mudar.huegoallight.io.model.base.** { *; }
-keep class ca.mudar.huegoallight.io.model.** { *; }

-dontwarn sun.misc.Unsafe
#-dontwarn com.google.common.collect.MinMaxPriorityQueue