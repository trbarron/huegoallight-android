/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight;

import android.os.Build;
import android.support.annotation.ColorInt;
import android.text.format.DateUtils;

import com.philips.lighting.hue.sdk.utilities.impl.Color;

public class Const {
    /**
     * Hue
     */
    public static final String HUE_APP_NAME = "GoalLight";
    public static final String HUE_WEBSITE = "http://www.meethue.com/";
    public static final int BLINK_SHORT_DURATION = 3; // seconds
    public static final int HUE_DEFAULT_TRANSITION = 400; // millis
    public static final int HUE_FAST_TRANSITION = 100; // millis

    /**
     * Hockey
     */
    public static long LIVE_REFRESH_DELAY = DateUtils.MINUTE_IN_MILLIS;
    public static long GAME_EXPIRY = 12 * DateUtils.HOUR_IN_MILLIS;
    public static long NOTIFY_MAX_DELAY = 3 * DateUtils.MINUTE_IN_MILLIS;
    @ColorInt
    public static int RED_COLOR = Color.RED;
    @ColorInt
    public static int ALTERNATE_RGB_COLOR = Color.WHITE;
    @ColorInt
    public static int DEFAULT_RGB_COLOR = RED_COLOR;
    public static String EAST_TIMEZONE = "America/New_York"; // TBD games are set at Eastern Midnight

    /**
     * Hockey API
     */
    public interface ApiPaths {
        // String BASE_URL = BuildConfig.API_BASE_URL; // "http://nhlapi.mudar.ca/api/"
        String GET_SEASON_CLENDAR = "calendar";
        String GET_CLUB_CLENDAR = "calendar/{" + ApiArgs.CLUB + "}";
        String GET_MULTI_LIVE_GAMES = "live/{" + ApiArgs.GAMES + "}";
        @Deprecated
        String GET_LIVE_GAME = "live"; // Of the API's default club
    }

    public interface ApiArgs {
        String TOKEN = "LiveApiKey";
        String CLUB = "club";
        String GAMES = "games_csv";
        String LAST_UPDATED_AT = "timestamp";
    }

    public interface ApiValues {
        String GAMECENTER_LINK = "gamecenter";
        String LINK_HREF = "href";
        String LINK_META = "meta";
    }

    /**
     * Google Cloud Messaging (GCM)
     */
    public static String[] GCM_DEFAULT_TOPICS = {"global"};

    /**
     * TabLayout
     */
    public interface MainTabs {
        int _COUNT = 2;
        int CURRENT = 0;
        int UPCOMING = 1;
    }

    /**
     * Lights grid columns
     */
    public interface LightsColumns {
        int _COUNT = 4;
        int UNUSED_LIGHTS = 0;
        int PRIMARY_COLOR = 1;
        int SECONDARY_COLOR = 2;
        int RED_LIGHT = 3;
        int _CENTER = PRIMARY_COLOR;
    }

    /**
     * Quiet hours fields
     */
    public interface TimePeriodFields {
        int START = 0;
        int END = 1;
    }

    // Assets
    public interface LocalAssets {
        String LICENSE = "gpl-3.0-standalone.html";
    }

    /**
     * Typefaces
     */
    public interface TypeFaces {
        int _COUNT = 1;
        String SCOREBOARD = "fonts/hockeyboard.ttf";
    }

    /**
     * Database
     */
    public static final String DATABASE_NAME = "goallight.db";
    public static final int DATABASE_VERSION = 6;

    /**
     * SharedPreferences
     */
    public static final String APP_PREFS_NAME = "goallight_prefs";

    public interface PrefsNames {
        // Hue Bridge
        String BRIDGE = "prefs_hue_bridge";
        String LAST_USERNAME = "prefs_last_username";
        String LAST_IP = "prefs_last_ip";
        String LAST_BRIDGE_ID = "prefs_last_bridge_id";
        String PRIMARY_LIGHTS = "prefs_selected_lights"; // "selected" is for legacy
        String SECONDARY_LIGHTS = "prefs_secondary_lights";
        String RED_LIGHTS = "prefs_red_lights";
        String BRIDGE_PRIMARY_LIGHTS = "prefs_%s_selected_lights"; // "selected" is for legacy
        String BRIDGE_SECONDARY_LIGHTS = "prefs_%s_secondary_lights";
        String BRIDGE_RED_LIGHTS = "prefs_%s_red_lights";
        String BLINK_DURATION = "prefs_blink_duration";

        // Google Cloud Messaging (GCM)
        String GCM_TOKEN = "gcm_token";

        // Quiet hours
        String QUIET_HOURS_ENABLED = "prefs_quiet_hours_enabled";
        String QUIET_HOURS_START = "prefs_quiet_hours_start";
        String QUIET_HOURS_END = "prefs_quiet_hours_end";

        // Lights board
        String LIGHTS_BOARD = "prefs_lights_board";

        // Notifications
        String RINGTONE = "prefs_ringtone";
        String HAS_VIBRATION = "prefs_has_vibration";
        String HAS_NOTIFICATIONS = "prefs_has_notifications";

        // NHL
        String NHL_FOLLOWED_TEAMS = "prefs_nhl_followed";
        String NHL_TEAM_PREFIX = "key_nhl_";
        String IGNORE_OPPONENT = "prefs_ignore_opponent";
        String SEASON_UPDATED_AT = "season_updated_at";

        // Onboarding
        String IS_ONBOARDING = "is_onboarding";
        String IS_LIGHTS_DISCOVERY = "is_lights_discovery";
    }

    public interface PrefsValues {
        String RINGTONE_SILENT = "";
        String BRIDGE_ANONYMOUS = "bridge";
        String DEFAULT_BLINK_DURATION = "6";
        boolean DEFAULT_QUIET_HOURS_ENABLED = false;
        int DEFAULT_QUIET_HOURS_START = 22 * 60; // minutesOfDay
        int DEFAULT_QUIET_HOURS_END = 7 * 60; // minutesOfDay
    }

    /**
     * Fragments
     */
    public interface FragmentTags {
        String PUSHLINK = "f_pushlink";
        String BRIDGES = "f_bridges";
        String LIGHTS_BOARD = "f_lights_board";
        String LIGHTS_BOARD_HELP = "f_lights_board_help";
        String GAME_DELAY_HELP = "f_game_delay_help";
        String TEAMS = "f_teams";
        String SETTINGS = "f_settings";
        String SETTINGS_NHL_TEAMS = "f_settings_nhl_teams";
        String SETTINGS_QUIET_HOURS = "f_settings_quiet_hours";
        String TIME_PICKER = "f_time_picker";
        String DELAY_SEEKBAR = "f_delay_seekbar";
    }

    /**
     * Request Codes
     */
    public interface RequestCodes {
        int NOTIFY_ID = 0;
        int HUE_SETUP = 10;
        int DELAY_SEEKBAR = 20;
        int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    }

    public interface IntentNames {
        String C2DM_RECEIVE = "com.google.android.c2dm.intent.RECEIVE";
    }

    /**
     * Bundle keys
     */
    public interface BundleKeys {
        String SELECTED_BRIDGE = "selected_bridge";
        String IS_ONBOARDING = "is_onboarding";
        String NB_ACCESS_POINTS = "nb_access_points";
        String GAME_ID = "game_id";
        String TEAM_SLUG = "team_slug";
        String IS_GCM_REGISTERED = "is_gcm_registered";
        String TOPIC_SUBSCRIBE = "topic_subscribe";
        String TIME_PERIOD_FIELD = "time_period_field";
        String HOUR_OF_DAY = "hour_of_day";
        String MINUTES = "minutes";
        String SECONDS = "seconds";
        String IS_DELAYED_CALLBACK = "is_delayed";

        // HueSetupActivity
        String ERROR_CODE = "error_code";
        String ERROR_MESSAGE = "error_message";
        String SHOW_PUSHLINK = "show_pushlink";
        String SHOW_BRIDGES_LIST = "show_bridges_list";
        String PROGRESSBAR_VISIBILITY = "progressbar_visibility";
        String PROGRESS_VALUE = "progress_value";
    }

    /**
     * Other constants
     */
    public static final int UNKNOWN_VALUE = -1;
    public static final long ANIM_SHORT_DURATION = 200L;
    public static final long ANIM_MEDIUM_DURATION = 400L;
    public static final long ANIM_LONG_DURATION = 500L;

    /**
     * Compatibility
     */
    public static boolean SUPPORTS_LOLLIPOP = android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

    /**
     * Possible cases:
     * - Onboarding not found
     * - Onboarding one found, auth required
     * - Onboarding list found
     * - Onboarding rotation: started twice?
     * - Main not found
     * - Main one found, auth required
     * - Main list found
     * - Setup, show list
     */
}
