/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.WorkerThread;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateUtils;

import com.google.android.gms.gcm.GcmListenerService;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.squareup.otto.Subscribe;

import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.BundleKeys;
import ca.mudar.huegoallight.Const.IntentNames;
import ca.mudar.huegoallight.Const.RequestCodes;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.DatabaseHelper;
import ca.mudar.huegoallight.data.GcmDataWrapper;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.GameSettings;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.ui.activity.MainActivity;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.HueBlinkHelper;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class MessagingListenerService extends GcmListenerService implements
        SyncBusListener {
    private static final String TAG = makeLogTag("MessagingListenerService");
    private static final int MAX_RETRIES = 12; // Retry Bridge connection 12 sec (default heartbeat is 10 sec)

    private boolean mIsBridgeConnected = false;
    private boolean mIsCacheUpdated = false;

    @Override
    public void onCreate() {
        super.onCreate();

        registerSyncBus();
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        mIsBridgeConnected = false;
        mIsCacheUpdated = false;

        final boolean isDelayedCallback = data.getBoolean(BundleKeys.IS_DELAYED_CALLBACK, false);

        try {
            final GcmDataWrapper gcmDataWrapper = GcmDataWrapper.fromJson(
                    data.getString("dataWrapper", null));

            final String message = gcmDataWrapper.getMessage();
            final List<Integer> colors = gcmDataWrapper.getColors();
            final long sentAt = gcmDataWrapper.getTimestamp();
            final NhlTeam team = gcmDataWrapper.getNhlTeam();
            final String gameId = gcmDataWrapper.getGame();

            final GameSettings gameSettings = DatabaseHelper.getGameSettings(getApplicationContext(), gameId);

            if (gameSettings.isMuted()) {
                // Skip muted game
                LOGV(TAG, "Game is on mute");
                return;
            }

            final HuePrefs prefs = HuePrefs.getInstance(getApplicationContext());

            // Message is silent when ignoring opponents's goals, or late notification
            final boolean isSilent = prefs.isCurrentlyQuietHours() ||
                    prefs.ignoreOpponentTeam(team.getSlug()) ||
                    (Math.abs(System.currentTimeMillis() - sentAt) > Const.NOTIFY_MAX_DELAY);

            if (isHueBridgeConnected(MAX_RETRIES)) {
                if (!isDelayedCallback) {
                    sendNotification(team, isSilent, gameSettings.hasDelay(), prefs);
                }

                if (!isSilent) {
                    if (isDelayedCallback || !gameSettings.hasDelay()) {
                        new HueBlinkHelper(getApplicationContext(), colors, team.getSlug()).startBlinking();
                    } else {
                        recallAfterDelay(data, gameSettings.getDelay());
                    }
                } else {
                    LOGV(TAG, "Silent notification");
                }
            }
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterSyncBus();
        HueBridgeUtils.disconnectBridgeIfNecessary(PHHueSDK.getInstance());
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param team
     * @param isSilent
     * @param prefs
     */
    private void sendNotification(NhlTeam team, boolean isSilent, boolean hasDelay, HuePrefs prefs) {
        if (!prefs.hasNotifications()) {
            return;
        }
        final boolean hasVibration = prefs.hasVibration();
        final Uri ringtone = prefs.getRingtonePath();

        final Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        final PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        final Resources res = getApplicationContext().getResources();
        final String contentTitle = res.getString(R.string.notify_goal_title);
        final String contentText = res.getString(R.string.notify_goal_text,
                res.getString(team.getTeamName()));

        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(team.getTeamNotifyIcon())
                .setColor(team.getColor())
                .setContentTitle(contentTitle)
                .setContentText(contentText)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setUsesChronometer(hasDelay);

        // Skip ringtone/vibration for silent games and for delayed games (to avoid spoiler)
        if (!isSilent && !hasDelay) {
            if (hasVibration && ringtone != null) {
                builder.setDefaults(Notification.DEFAULT_VIBRATE)
                        .setSound(ringtone, AudioManager.STREAM_NOTIFICATION);
            } else if (hasVibration) {
                builder.setDefaults(Notification.DEFAULT_VIBRATE);
            } else if (ringtone != null) {
                builder.setSound(ringtone);
            }
        }

        final NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(RequestCodes.NOTIFY_ID, builder.build());
    }

    /**
     * Check if the Bridge is connected. Has a wait loop, to handle the bridge re-connection delay
     *
     * @param maxRetry number of retries, each waits a second
     * @return true if bridge is connected
     */
    @WorkerThread
    private boolean isHueBridgeConnected(final int maxRetry) {
        final PHHueSDK hueSDK = PHHueSDK.getInstance();
        final HueSDKListener listener = HueBridgeUtils.initializeHueBridge(new ContextWrapper(this), hueSDK);
        listener.setFastHeartBeatInterval();

        int retry = 0;
        while (retry < maxRetry && !mIsCacheUpdated) {
            SystemClock.sleep(DateUtils.SECOND_IN_MILLIS);
            retry++;
        }
        return mIsBridgeConnected;
    }

    @Subscribe
    public void onBridgeConnected(SyncBusEvents.BridgeConnected event) {
        mIsBridgeConnected = true;
    }

    @Subscribe
    public void onBridgeConnectionResumed(SyncBusEvents.BridgeConnectionResumed event) {
        mIsBridgeConnected = true;
    }

    @Subscribe
    public void onCacheUpdated(SyncBusEvents.HueCacheUpdated event) {
        mIsCacheUpdated = true;
        mIsBridgeConnected = true; // updated cache means connected bridge
    }

    private void recallAfterDelay(Bundle data, int seconds) {
        final Context context = getApplicationContext();
        final Intent intent = new Intent(context, MessagingListenerService.class);
        intent.setAction(IntentNames.C2DM_RECEIVE);
        data.putBoolean(BundleKeys.IS_DELAYED_CALLBACK, Boolean.TRUE);
        intent.putExtras(data);

        final PendingIntent pi = PendingIntent.getService(context, 0, intent, 0);

        final long triggerAt = System.currentTimeMillis() + (seconds * DateUtils.SECOND_IN_MILLIS);
        ((AlarmManager) context.getSystemService(Context.ALARM_SERVICE))
                .setExact(AlarmManager.RTC_WAKEUP, triggerAt, pi);
    }
}
