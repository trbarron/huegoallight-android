/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.database.Cursor;
import android.os.Handler;
import android.service.dreams.DreamService;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.data.Queries;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.provider.EventsContract;
import ca.mudar.huegoallight.ui.view.FloatingFrameLayout;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class TeamsDaydream extends DreamService {
    private static final String TAG = makeLogTag("TeamsDaydream");

    private static final long DAYDREAM_DURATION = DateUtils.MINUTE_IN_MILLIS; // Ref: equals R.dimen.daydream_duration
    private static final long FADE_DURATION = 500;

    private List<String> mFollowedTeams;
    private final Handler mHandler = new Handler();

    @Override
    public void onDreamingStarted() {
        super.onDreamingStarted();
    }

    @Override
    public void onDreamingStopped() {
        super.onDreamingStopped();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        setInteractive(false);
        setFullscreen(true);

        setContentView(R.layout.daydream);

        mFollowedTeams = HuePrefs.getInstance(getApplicationContext()).getFollowedNhlTeams();
        updateLogos();
    }


    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        mHandler.removeCallbacksAndMessages(null);
    }

    private void updateLogos() {
        final Cursor cursor = getApplicationContext().getContentResolver().query(
                EventsContract.Games.CONTENT_URI_CURRENT,
                Queries.CurrentGamesQuery.PROJECTION,
                EventsContract.Games.getSelectionByTeams(mFollowedTeams),
                null,
                EventsContract.Games.RANDOM_SORT
        );

        if (cursor != null && cursor.moveToFirst()) {
            final String awaySlug = cursor.getString(Queries.CurrentGamesQuery.AWAY_TEAM_SLUG);
            final String homeSlug = cursor.getString(Queries.CurrentGamesQuery.HOME_TEAM_SLUG);
            final long startsAt = cursor.getLong(Queries.CurrentGamesQuery.STARTS_AT);
            final int awayScore = cursor.getInt(Queries.CurrentGamesQuery.AWAY_TEAM_SCORE);
            final int homeScore = cursor.getInt(Queries.CurrentGamesQuery.HOME_TEAM_SCORE);

            final NhlTeam awayTeam = new NhlTeam(awaySlug);
            final NhlTeam homeTeam = new NhlTeam(homeSlug);

            if (Long.compare(startsAt, System.currentTimeMillis()) < 0) {
                showTeamsWithScore(awayTeam, homeTeam, awayScore, homeScore);
            } else {
                showTeams(awayTeam, homeTeam);
            }

            cursor.close();
        } else {
            // No current games, show the logo of a random followed team
            final int nbFollowedTeams = mFollowedTeams.size();
            if (nbFollowedTeams > 0) {
                final String slug = mFollowedTeams.get(new Random().nextInt(nbFollowedTeams));
                showTeam(new NhlTeam(slug));
            }
        }

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateLogos();
                ((FloatingFrameLayout) findViewById(R.id.floating_frame)).jump();
            }
        }, DAYDREAM_DURATION + FADE_DURATION);
    }

    /**
     * Show single team (home)
     *
     * @param homeTeam
     */
    private void showTeam(NhlTeam homeTeam) {
        showTeamsWithScore(null, homeTeam, Const.UNKNOWN_VALUE, Const.UNKNOWN_VALUE);
    }

    /**
     * Show Teams logos without score
     *
     * @param awayTeam
     * @param homeTeam
     */
    private void showTeams(NhlTeam awayTeam, NhlTeam homeTeam) {
        showTeamsWithScore(awayTeam, homeTeam, Const.UNKNOWN_VALUE, Const.UNKNOWN_VALUE);
    }

    /**
     * Show Teams logos with score
     *
     * @param awayTeam
     * @param homeTeam
     * @param awayScore
     * @param homeScore
     */
    private void showTeamsWithScore(NhlTeam awayTeam, NhlTeam homeTeam, int awayScore, int homeScore) {
        ((ImageView) findViewById(R.id.home_logo)).setImageResource(homeTeam.getTeamLogo());

        final ImageView vAwayLogo = (ImageView) findViewById(R.id.away_logo);
        final TextView vAwayScore = (TextView) findViewById(R.id.away_score);
        final TextView vHomeScore = (TextView) findViewById(R.id.home_score);

        if (awayTeam == null) {
            // One team only
            vAwayLogo.setVisibility(View.GONE);
        } else {
            // Two teams
            vAwayLogo.setVisibility(View.VISIBLE);
            ((ImageView) findViewById(R.id.away_logo)).setImageResource(awayTeam.getTeamLogo());
        }

        if ((awayScore != Const.UNKNOWN_VALUE) && (homeScore != Const.UNKNOWN_VALUE)) {
            // Game with score
            vAwayScore.setVisibility(View.VISIBLE);
            vHomeScore.setVisibility(View.VISIBLE);
            vAwayScore.setText(String.valueOf(awayScore));
            vHomeScore.setText(String.valueOf(homeScore));
        } else {
            // Upcoming game, without score
            vAwayScore.setVisibility(View.GONE);
            vHomeScore.setVisibility(View.GONE);
        }
    }
}
