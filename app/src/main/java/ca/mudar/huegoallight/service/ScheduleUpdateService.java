/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import java.util.List;

import ca.mudar.huegoallight.data.DatabaseHelper;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.io.ApiClient;
import ca.mudar.huegoallight.io.model.NhlGamesList;
import ca.mudar.huegoallight.io.model.NhlGamesListMeta;
import ca.mudar.huegoallight.model.NhlGame;
import ca.mudar.huegoallight.utils.ConnectionUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGI;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class ScheduleUpdateService extends IntentService {
    private static final String TAG = makeLogTag("ScheduleUpdateService ");

    public static Intent newIntent(Context context) {
        return new Intent(context, ScheduleUpdateService.class);
    }

    public ScheduleUpdateService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!ConnectionUtils.hasConnection(getApplicationContext())) {
            return;
        }

        final long startTime = System.currentTimeMillis();

        final HuePrefs prefs = HuePrefs.getInstance(getApplicationContext());

        final NhlGamesList games = ApiClient.getSeasonCalendar(ApiClient.getService(), prefs.getScheduleLastUpdatedAt());
        if (games != null) {
            final List<NhlGame> data = games.getData();
            final NhlGamesListMeta meta = games.getMeta();

            if (data != null && data.size() > 0) {
                LOGI(TAG, "Updating season schedule...");
                // Update database and set lastUpdated timestamp
                DatabaseHelper.addGamesToSchedule(getApplicationContext(), data);
                prefs.setScheduleLastUpdatedAt(meta.getTimestamp());
            } else if (meta.isDataFiltered()) {
                // Data is empty thanks to timestamp filter, update local timestamp value
                prefs.setScheduleLastUpdatedAt(meta.getTimestamp());
            }
        }

        LOGI(TAG, String.format("Sync duration: %d ms", System.currentTimeMillis() - startTime));
    }
}
