/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.GcmUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGE;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class TopicsUpdateService extends IntentService {
    private static final String TAG = makeLogTag("TopicsUpdateService");

    public static Intent newIntent(Context context, String topic, boolean subscribe) {
        final Intent intent = new Intent(context, TopicsUpdateService.class);

        final Bundle extras = new Bundle();
        extras.putString(Const.BundleKeys.TEAM_SLUG, topic);
        extras.putBoolean(Const.BundleKeys.TOPIC_SUBSCRIBE, subscribe);
        intent.putExtras(extras);

        return intent;
    }

    public TopicsUpdateService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (!ConnectionUtils.hasConnection(getApplicationContext())) {
            return;
        }

        final String team = intent.getStringExtra(Const.BundleKeys.TEAM_SLUG);
        final boolean subscribe = intent.getBooleanExtra(Const.BundleKeys.TOPIC_SUBSCRIBE, false);
        if (TextUtils.isEmpty(team)) {
            return;
        }

        final String token = getGcmToken();
        try {
            if (subscribe) {
                GcmUtils.subscribeTeam(getApplicationContext(), token, team);
            } else {
                GcmUtils.unsubscribeTeam(getApplicationContext(), token, team);
            }
        } catch (Exception e) {
            e.printStackTrace();
            HueGoalApp.getSyncBus().post(new SyncBusEvents.GcmTopicsError(team, e));
        }

        HueGoalApp.getSyncBus().post(new SyncBusEvents.GcmTopicsUpdated());
    }

    /**
     * Get GCM token from SharedPreferences, or do a remote call if necessary
     *
     * @return
     */
    private String getGcmToken() {
        String token = HuePrefs.getInstance(getApplicationContext())
                .getGcmToken();

        if (TextUtils.isEmpty(token)) {
            // Call InstanceID if necessary
            try {
                token = InstanceID.getInstance(this)
                        .getToken(getString(R.string.gcm_sender_id),
                                GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                                null);

                HuePrefs.getInstance(getApplicationContext())
                        .setGcmToken(token);
            } catch (Exception e) {
                LOGE(TAG, "Failed to complete token refresh", e);
                HueGoalApp.getSyncBus().post(new SyncBusEvents.GcmRegistrationError(e));
            }
        }

        return token;
    }
}
