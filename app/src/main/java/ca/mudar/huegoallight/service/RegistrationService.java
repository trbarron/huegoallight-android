/*
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.mudar.huegoallight.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.utils.GcmUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class RegistrationService extends IntentService {
    private static final String TAG = makeLogTag("RegistrationService");

    public static Intent newIntent(Context context) {
        return new Intent(context, RegistrationService.class);
    }

    public RegistrationService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            /**
             * Initially this call goes out to the network to retrieve the token,
             * subsequent calls are local. For details on R.string.gcm_defaultSenderId, see
             * https://developers.google.com/cloud-messaging/android/start
             */
            final String token = InstanceID.getInstance(this)
                    .getToken(getString(R.string.gcm_sender_id),
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE,
                            null);
            HuePrefs.getInstance(getApplicationContext())
                    .setGcmToken(token);

            HueGoalApp.getSyncBus().post(new SyncBusEvents.GcmRegistered());

            // Subscribe to global notifications
            GcmUtils.subscribeDefaultTopics(this, token);
        } catch (Exception e) {
            HueGoalApp.getSyncBus().post(new SyncBusEvents.GcmRegistrationError(e));
            e.printStackTrace();
        }
    }

}
