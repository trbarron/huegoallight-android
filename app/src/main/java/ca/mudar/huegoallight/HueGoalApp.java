/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;
import io.fabric.sdk.android.Fabric;

public class HueGoalApp extends Application {
    private PHHueSDK hueSDK;
    private HueSDKListener hueListener;

    private static final Bus SYNC_BUS = new Bus(ThreadEnforcer.ANY);

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.USE_CRASHLYTICS) {
            Fabric.with(this, new Crashlytics());
        }

        HuePrefs.setDefaults(this);

        initializeHueSDK();

        hueListener = new HueSDKListener(this);
        hueSDK.getNotificationManager().registerSDKListener(hueListener);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        hueSDK.getNotificationManager().unregisterSDKListener(hueListener);
    }

    private void initializeHueSDK() {
        hueSDK = PHHueSDK.create();

        // Set the App/Device Name, to be stored in the bridge's white-list
        hueSDK.setAppName(Const.HUE_APP_NAME);
        hueSDK.setDeviceName(android.os.Build.MODEL);
    }

    public static Bus getSyncBus() {
        return SYNC_BUS;
    }

    public HueSDKListener getHueListener() {
        return hueListener;
    }
}
