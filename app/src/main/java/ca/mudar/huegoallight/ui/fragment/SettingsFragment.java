/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.TextUtils;

import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.ui.activity.HueSetupActivity;
import ca.mudar.huegoallight.ui.activity.LightsBoardActivity;
import ca.mudar.huegoallight.ui.activity.SettingsNhlTeamsActivity;
import ca.mudar.huegoallight.ui.activity.SettingsQuietHoursActivity;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.TimeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class SettingsFragment extends PreferenceFragment implements
        Const.PrefsNames,
        Const.PrefsValues,
        Preference.OnPreferenceClickListener,
        SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = makeLogTag("SettingsFragment");

    private SharedPreferences mSharedPrefs;
    private RingtonePreference mRingtone;
    private Preference mBridge;
    private Preference mLightsBoard;
    private Preference mBlinkDuration;
    private List<PHLight> mAllLights;

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PreferenceManager pm = this.getPreferenceManager();
        pm.setSharedPreferencesName(Const.APP_PREFS_NAME);
        pm.setSharedPreferencesMode(Context.MODE_PRIVATE);

        addPreferencesFromResource(R.xml.prefs_settings);
        mRingtone = (RingtonePreference) findPreference(Const.PrefsNames.RINGTONE);
        mLightsBoard = findPreference(LIGHTS_BOARD);
        mBlinkDuration = findPreference(BLINK_DURATION);
        mBridge = findPreference(BRIDGE);

        mSharedPrefs = pm.getSharedPreferences();

        /**
         * Set up a listener whenever a key changes
         */
        mSharedPrefs.registerOnSharedPreferenceChangeListener(this);

        findPreference(NHL_FOLLOWED_TEAMS).setOnPreferenceClickListener(this);
        findPreference(QUIET_HOURS_ENABLED).setOnPreferenceClickListener(this);
        mLightsBoard.setOnPreferenceClickListener(this);
        mBridge.setOnPreferenceClickListener(this);

        removeVibrationIfNotSupported();

        try {
            mAllLights = PHHueSDK.getInstance().getSelectedBridge().getResourceCache().getAllLights();
        } catch (NullPointerException e) {
            mAllLights = new ArrayList<>();
        }
        mLightsBoard.setEnabled(mAllLights.size() > 0);
    }

    @Override
    public void onResume() {
        super.onResume();

        setupSummaries();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        /**
         * Remove the listener
         */
        if (mSharedPrefs != null) {
            mSharedPrefs.unregisterOnSharedPreferenceChangeListener(this);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (RINGTONE.equals(key)) {
            mRingtone.setSummary(getRingtoneSummary());
        } else if (BLINK_DURATION.equals(key)) {
            mBlinkDuration.setSummary(getBlinkDurationSummary());
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        final String key = preference.getKey();

        if (QUIET_HOURS_ENABLED.equals(key)) {
            startActivity(SettingsQuietHoursActivity.newIntent(getActivity()));
            return true;
        }

        if (!ConnectionUtils.checkConnection(getActivity(), getView())) {
            return true;
        } else if (NHL_FOLLOWED_TEAMS.equals(key)) {
            startActivity(SettingsNhlTeamsActivity.newIntent(getActivity()));
            return true;
        } else if (BRIDGE.equals(key)) {
            startActivity(HueSetupActivity.newBridgeSelectorIntent(getActivity()));
            return true;
        } else if (LIGHTS_BOARD.equals(key)) {
            startActivity(LightsBoardActivity.newIntent(getActivity()));
            return true;
        }
        return false;
    }

    private void setupSummaries() {
        mRingtone.setSummary(getRingtoneSummary());
        mLightsBoard.setSummary(getSelectedLightsSummary(mAllLights));
        mBlinkDuration.setSummary(getBlinkDurationSummary());
        mBridge.setSummary(getBridgeSummary(PHHueSDK.getInstance().getSelectedBridge()));

        findPreference(NHL_FOLLOWED_TEAMS).setSummary(getTeamsSummary());
        findPreference(QUIET_HOURS_ENABLED).setSummary(getQuietHoursSummary());
    }

    private String getTeamsSummary() {
        final Resources res = getResources();
        final String summary;

        final List<String> teams = HuePrefs.getInstance(getActivity().getApplicationContext())
                .getFollowedNhlTeams();
        final int nbTeams = teams.size();
        if (nbTeams == 0) {
            // Not following any teams
            summary = res.getString(R.string.prefs_summary_teams_followed_none);
        } else if (nbTeams == 1) {
            // Following a single team: full neam
            summary = res.getString(new NhlTeam(teams.get(0)).getTeamName());
        } else {
            // Following multiple teams: short team names
            final ArrayList<String> teamNames = new ArrayList<>();
            for (String slug : teams) {
                teamNames.add(res.getString(new NhlTeam(slug).getTeamShortName()));
            }
            summary = TextUtils.join(res.getString(R.string.prefs_summary_list_delimeter),
                    teamNames);
        }

        return summary;
    }

    private String getQuietHoursSummary() {
        final Context context = getActivity().getApplicationContext();

        if (mSharedPrefs.getBoolean(QUIET_HOURS_ENABLED, DEFAULT_QUIET_HOURS_ENABLED)) {
            final String start = TimeUtils.getTimeDisplay(context,
                    mSharedPrefs.getInt(QUIET_HOURS_START, DEFAULT_QUIET_HOURS_START));
            final String end = TimeUtils.getTimeDisplay(context,
                    mSharedPrefs.getInt(QUIET_HOURS_END, DEFAULT_QUIET_HOURS_END));

            return getResources().getString(R.string.prefs_summary_quiet_hours_period, start, end);
        }

        return getResources().getString(R.string.prefs_summary_quiet_hours_off);
    }

    private String getRingtoneSummary() {
        final String path = mSharedPrefs.getString(RINGTONE, RINGTONE_SILENT);
        if (!TextUtils.isEmpty(path)) {
            final Ringtone ringtone = RingtoneManager.getRingtone(getActivity(), Uri.parse(path));
            if (ringtone == null) {
                // revert to silent ringtone
                mSharedPrefs.edit().putString(RINGTONE, RINGTONE_SILENT).apply();
            } else {
                return ringtone.getTitle(getActivity().getApplicationContext());
            }
        }

        return getResources().getString(R.string.prefs_summary_ringtone_silent);
    }

    private String getBlinkDurationSummary() {
        final String duration = mSharedPrefs.getString(BLINK_DURATION, DEFAULT_BLINK_DURATION);

        return getResources().getString(R.string.prefs_summary_hue_blink_duration, duration);
    }

    private void removeVibrationIfNotSupported() {
        final Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        if (!v.hasVibrator()) {
            ((CheckBoxPreference) findPreference(HAS_VIBRATION)).setChecked(false);
            getPreferenceScreen().removePreference(findPreference(Const.PrefsNames.HAS_VIBRATION));
        }
    }

    private String getSelectedLightsSummary(List<PHLight> allLights) {
        if (allLights == null || allLights.size() == 0) {
            return getResources().getString(R.string.prefs_summary_hue_available_lights_error);
        }
        final List<String> selectedLights = HuePrefs.getInstance(getActivity().getApplicationContext())
                .getAllSelectedLights();
        final List<String> summaryNames = new ArrayList<>();

        for (PHLight light : allLights) {
            if (selectedLights.contains(light.getUniqueId())) {
                summaryNames.add(light.getName());
            }
        }

        if (summaryNames.size() == 0) {
            return getResources().getString(R.string.prefs_summary_hue_selected_lights_none);
        } else {
            Collections.sort(summaryNames);
            return TextUtils.join(getResources().getString(R.string.prefs_summary_list_delimeter),
                    summaryNames);
        }
    }

    private String getBridgeSummary(PHBridge bridge) {
        try {
            return HueBridgeUtils.getBridgeName(
                    bridge.getResourceCache().getBridgeConfiguration().getBridgeID());
        } catch (NullPointerException e) {
            return getResources().getString(R.string.prefs_summary_hue_bridge_error);
        }
    }
}
