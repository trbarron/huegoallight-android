/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHMessageType;
import com.philips.lighting.hue.sdk.exception.PHHueException;
import com.philips.lighting.model.PHHueError;
import com.squareup.otto.Subscribe;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.ui.adapter.BridgesAdapter;
import ca.mudar.huegoallight.ui.fragment.BridgesListFragment;
import ca.mudar.huegoallight.ui.fragment.PushlinkFragment;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class HueSetupActivity extends AppCompatActivity implements
        SyncBusListener,
        BridgesAdapter.OnBridgeSelectedListener {
    private static final String TAG = makeLogTag("HueSetupActivity");

    // Enforce singleTop to avoid multi-instances. Related to quick BusSync events received before unregistering
    private static final int INTENT_FLAGS = Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP;

    private PHHueSDK hueSDK;
    private HueSDKListener mHueSDKListener;
    private View vProgress;
    private TextView vError;
    private FloatingActionButton vFab;
    private int mLastErrorCode;
    private boolean mAutoConnect;

    public static Intent newOnboardingIntent(Context context, int nbAccessPoints) {
        final Intent intent = new Intent(context, HueSetupActivity.class);
        intent.setFlags(INTENT_FLAGS);

        final Bundle extras = new Bundle();
        extras.putBoolean(Const.BundleKeys.IS_ONBOARDING, true);
        extras.putBoolean(Const.BundleKeys.SHOW_BRIDGES_LIST, nbAccessPoints > 1);
        intent.putExtras(extras);

        return intent;
    }

    public static Intent newAuthenticationIntent(Context context) {
        final Intent intent = new Intent(context, HueSetupActivity.class);
        intent.setFlags(INTENT_FLAGS);

        final Bundle extras = new Bundle();
        extras.putBoolean(Const.BundleKeys.SHOW_PUSHLINK, true);
        intent.putExtras(extras);

        return intent;
    }

    public static Intent newErrorIntent(Context context, SyncBusEvents.BridgeError error) {
        final Intent intent = new Intent(context, HueSetupActivity.class);
        intent.setFlags(INTENT_FLAGS);

        final Bundle extras = new Bundle();
        extras.putInt(Const.BundleKeys.ERROR_CODE, error.getCode());
        extras.putString(Const.BundleKeys.ERROR_MESSAGE, error.getMessage());
        intent.putExtras(extras);

        return intent;
    }

    public static Intent newBridgeSelectorIntent(Context context) {
        final Intent intent = new Intent(context, HueSetupActivity.class);
        intent.setFlags(INTENT_FLAGS);

        final Bundle extras = new Bundle();
        extras.putBoolean(Const.BundleKeys.SHOW_BRIDGES_LIST, true);
        intent.putExtras(extras);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hue_setup);

        vProgress = findViewById(R.id.progress);
        vError = (TextView) findViewById(R.id.hue_error);
        vFab = (FloatingActionButton) findViewById(R.id.fab);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        final boolean showPushlink = getIntent().getBooleanExtra(Const.BundleKeys.SHOW_PUSHLINK, false);
        final boolean showBridgesList = getIntent().getBooleanExtra(Const.BundleKeys.SHOW_BRIDGES_LIST, false);
        final boolean isOnboarding = getIntent().getBooleanExtra(Const.BundleKeys.IS_ONBOARDING, false);
        final int errorCode = getIntent().getIntExtra(Const.BundleKeys.ERROR_CODE, Const.UNKNOWN_VALUE);
        final String errorMessage = getIntent().getStringExtra(Const.BundleKeys.ERROR_MESSAGE);
        mAutoConnect = !showBridgesList;

        if (savedInstanceState == null) {
            if (showPushlink) {
                showPushLinkFragment(false);
            } else if (showBridgesList) {
                showBridgesListFragment();
            }
        }

        setupToolbar(isOnboarding);
        setupFab();
        setupHueBridge(mAutoConnect);
        showErrorIfNecessary(errorCode, errorMessage);
    }

    @Override
    protected void onStart() {
        super.onStart();

        registerSyncBus();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.BundleKeys.ERROR_CODE, mLastErrorCode);
        outState.putInt(Const.BundleKeys.PROGRESSBAR_VISIBILITY, vProgress.getVisibility());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mLastErrorCode = savedInstanceState.getInt(Const.BundleKeys.ERROR_CODE, Const.UNKNOWN_VALUE);

        final int visibility = savedInstanceState.getInt(Const.BundleKeys.PROGRESSBAR_VISIBILITY, View.VISIBLE);
        //noinspection ResourceType
        vProgress.setVisibility(visibility);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterSyncBus();

        try {
            hueSDK.stopPushlinkAuthentication();
// TODO verify if needed
//            hueSDK.getNotificationManager().cancelSearchNotification();
        } catch (PHHueException e) {
            e.printStackTrace();
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements BridgesAdapter.OnBridgeSelectedListener
     *
     * @param id
     */
    @Override
    public void onBridgeSelected(int id) {
        try {
            mHueSDKListener.onAccessPointSelected(hueSDK.getAccessPointsFound().get(id));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Subscribe
    public void onBridgeAuthRequired(SyncBusEvents.BridgeAuthRequired event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showPushLinkFragment(true);
            }
        });
    }

    @Subscribe
    public void onAccessPointFound(SyncBusEvents.AccessPointFound event) {
        if (event.isAutoConnect()) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showPushLinkFragment(true);
                    onBridgeSelected(0);
                }
            });
        }
    }

    @Subscribe
    public void onMultiAccessPointsFound(SyncBusEvents.MultiAccessPointsFound event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showBridgesListFragment();
            }
        });
    }

    @Subscribe
    public void onBridgeConnectionResumed(SyncBusEvents.BridgeConnectionResumed event) {
        if (isFinishing()) {
            // to avoid delay after finish() call from onBridgeConnected()
            return;
        }

        hueSDK.disableHeartbeat(hueSDK.getSelectedBridge());
        hueSDK.disconnect(hueSDK.getSelectedBridge());

        // Start the UPNP search of local bridges.
        PHBridgeSearchManager sm = (PHBridgeSearchManager) hueSDK.getSDKService(PHHueSDK.SEARCH_BRIDGE);
        sm.search(true, true);
    }

    @Subscribe
    public void onBridgeConnected(SyncBusEvents.BridgeConnected event) {
        setResult(Activity.RESULT_OK);
        finish();
    }

    @Subscribe
    public void onBridgeError(final SyncBusEvents.BridgeError error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showErrorIfNecessary(error.getCode(), error.getMessage());
                mLastErrorCode = error.getCode();
            }
        });
    }

    @Subscribe
    public void onPushLinkWarn(final SyncBusEvents.PushlinkWarn error) {
        mLastErrorCode = PHMessageType.PUSHLINK_BUTTON_NOT_PRESSED;
    }

    /**
     * Setup Hue SDK and Listener.
     *
     * @param autoConnect Opposite of showBridgesList
     */
    private void setupHueBridge(boolean autoConnect) {
        hueSDK = PHHueSDK.getInstance();
        mHueSDKListener = HueBridgeUtils.initializeHueBridge(new ContextWrapper(this), hueSDK, !autoConnect);
        mHueSDKListener.setSingleBridgeAutoConnect(autoConnect);
    }

    private void setupToolbar(boolean isOnboarding) {
        getSupportActionBar().setDisplayShowTitleEnabled(!isOnboarding);
        getSupportActionBar().setDisplayHomeAsUpEnabled(!isOnboarding);
        findViewById(R.id.onboarding_title).setVisibility(isOnboarding ? View.VISIBLE : View.GONE);
    }

    private void setupFab() {
        vFab.hide();
        vFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retryBridgeConnection();
            }
        });
    }

    private void showPushLinkFragment(boolean forceTransaction) {
        if (isFinishing()) {
            return;
        }

        final FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag(Const.FragmentTags.PUSHLINK) == null) {
            // This can be called through intent and hueSDK callbacks, so show only once
            final Fragment fragment = PushlinkFragment.newInstance();
            final FragmentTransaction ft = fm.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_container, fragment, Const.FragmentTags.PUSHLINK);
            if (forceTransaction) {
                ft.commitNow();
            } else {
                ft.commit();
            }
            vProgress.setVisibility(View.GONE);
        }
    }

    private void showBridgesListFragment() {
        if (isFinishing()) {
            return;
        }

        final FragmentManager fm = getSupportFragmentManager();
        if (fm.findFragmentByTag(Const.FragmentTags.BRIDGES) == null) {
            // This can be called through intent and hueSDK callbacks, so show only once
            final Fragment fragment = BridgesListFragment.newInstance();
            fm.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .replace(R.id.fragment_container, fragment, Const.FragmentTags.BRIDGES)
                    .commit();
            vProgress.setVisibility(View.GONE);
        }
    }

    private void showErrorIfNecessary(int code, String message) {
        if (code == PHHueError.BRIDGE_ALREADY_CONNECTED) {
            onBridgeConnected(null);
            return;
        }

        final View view = findViewById(android.R.id.content);
        if (code == -1 || code == PHMessageType.PUSHLINK_BUTTON_NOT_PRESSED || view == null) {
            mLastErrorCode = code;
            return;
        }

        // Show the Snackbar message
        final boolean buttonNotPressed = (mLastErrorCode == PHMessageType.PUSHLINK_BUTTON_NOT_PRESSED)
                && (code == PHMessageType.PUSHLINK_AUTHENTICATION_FAILED);
        if (buttonNotPressed) {
            Snackbar.make(view.findViewById(R.id.main_content), R.string.snackbar_pushlink_error, Snackbar.LENGTH_LONG)
                    .show();
        }
// TODO: verify if still needed?
        hueSDK.stopPushlinkAuthentication();

        // Remove Bridges/Pushlink fragments
        removeAllFragments();

        // Hide Progressbar
        vProgress.setVisibility(View.GONE);

        // Setup the screen's error message and fadeIn
        vError.setText(message);
        if (vError.getVisibility() == View.GONE) {
            vError.setAlpha(0f);
            vError.animate()
                    .alpha(1f)
                    .setDuration(Const.ANIM_SHORT_DURATION)
                    .setStartDelay(Const.ANIM_MEDIUM_DURATION)
                    .start();
            vError.setVisibility(View.VISIBLE);
        }
        vFab.show();
    }

    private void removeAllFragments() {
        final FragmentManager fm = getSupportFragmentManager();
        final Fragment fragment1 = fm.findFragmentByTag(Const.FragmentTags.PUSHLINK);
        final Fragment fragment2 = fm.findFragmentByTag(Const.FragmentTags.BRIDGES);

        if (fragment1 == null && fragment2 == null) {
            return;
        }

        final FragmentTransaction ft = fm.beginTransaction();
        ft.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        if (fragment1 != null) {
            ft.remove(fragment1);
        }
        if (fragment2 != null) {
            ft.remove(fragment2);
        }
        ft.commit();
    }

    private void retryBridgeConnection() {
        vError.setVisibility(View.GONE);
        vFab.hide();
        vProgress.setVisibility(View.VISIBLE);

        setupHueBridge(mAutoConnect);
    }
}
