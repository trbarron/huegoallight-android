/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.listener;

public interface SyncBusEvents {
    class LightBlinkStarted {
    }

    class LightBlinkStopped {
    }

    class HueCacheUpdated {
    }

    class AccessPointFound {
        private boolean autoConnect;

        public AccessPointFound(boolean autoConnect) {
            this.autoConnect = autoConnect;
        }

        public boolean isAutoConnect() {
            return autoConnect;
        }
    }

    class MultiAccessPointsFound {
    }

    class BridgeConnected {
    }

    class BridgeAuthRequired {
    }

    class BridgeConnectionResumed {
    }

    class BridgeConnectionLost {
    }

    class GcmTopicsUpdated {
    }

    class GcmTopicsError {
        private String team;
        private Exception exception;

        public GcmTopicsError(String team, Exception exception) {
            this.team = team;
            this.exception = exception;
        }

        public String getTeam() {
            return team;
        }

        public Exception getException() {
            return exception;
        }
    }

    class GcmRegistered {
    }

    class GcmRegistrationError {
        private Exception exception;

        public GcmRegistrationError(Exception exception) {
            this.exception = exception;
        }

        public Exception getException() {
            return exception;
        }
    }

    class BridgeError {
        private int code;
        private String message;

        public BridgeError(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public String getMessage() {
            return message;
        }
    }

    class PushlinkWarn {
    }

    class LightsSelectionError {
    }
}
