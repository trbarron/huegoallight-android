/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TimePicker;

import java.util.Calendar;

import ca.mudar.huegoallight.Const.BundleKeys;
import ca.mudar.huegoallight.Const.TimePeriodFields;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.TimePeriodField;
import ca.mudar.huegoallight.utils.TimeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class TimePickerFragment extends DialogFragment implements
        TimePickerDialog.OnTimeSetListener {
    private static final String TAG = makeLogTag("TimePickerFragment");

    // @TimeRangeField
    private int timeRangeField;

    public static TimePickerFragment newInstance(@TimePeriodField int field, Calendar calendar) {
        TimePickerFragment fragment = new TimePickerFragment();

        final Bundle args = new Bundle();
        args.putInt(BundleKeys.TIME_PERIOD_FIELD, field);
        args.putInt(BundleKeys.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY));
        args.putInt(BundleKeys.MINUTES, calendar.get(Calendar.MINUTE));
        fragment.setArguments(args);

        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.timeRangeField = getArguments().getInt(BundleKeys.TIME_PERIOD_FIELD);
        final int hourOfDay = getArguments().getInt(BundleKeys.HOUR_OF_DAY);
        final int minutes = getArguments().getInt(BundleKeys.MINUTES);

        final TimePickerDialog dialog = new TimePickerDialog(getActivity(),
                R.style.AppTheme_Dialog,
                this,
                hourOfDay,
                minutes,
                false);
        dialog.setTitle(null);

        return dialog;
    }

    /**
     * Implements TimePickerDialog.OnTimeSetListener
     *
     * @param view
     * @param hourOfDay
     * @param minute
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (timeRangeField == TimePeriodFields.START) {
            HuePrefs.getInstance(getActivity().getApplicationContext())
                    .setQuietHoursStart(TimeUtils.getMinutesOfDay(hourOfDay, minute));
        } else if (timeRangeField == TimePeriodFields.END) {
            HuePrefs.getInstance(getActivity().getApplicationContext())
                    .setQuietHoursEnd(TimeUtils.getMinutesOfDay(hourOfDay, minute));
        }
    }
}
