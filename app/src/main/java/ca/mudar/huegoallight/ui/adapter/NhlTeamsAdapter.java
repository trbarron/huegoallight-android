/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.model.NhlTeam;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class NhlTeamsAdapter extends RecyclerView.Adapter<NhlTeamsAdapter.TeamViewHolder> {
    private static final String TAG = makeLogTag("NhlTeamsAdapter");

    protected final Resources resources;
    @LayoutRes
    private final int itemLayout;
    protected final OnTeamSelectedListener listener;

    private List<NhlTeam> mDataset;
    private boolean mEnableOnItemClick;

    public NhlTeamsAdapter(Context context, @LayoutRes int itemLayout, OnTeamSelectedListener listener) {
        this.resources = context.getResources();
        this.itemLayout = itemLayout;
        this.listener = listener;
        this.mEnableOnItemClick = true;
    }

    @Override
    public TeamViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new TeamViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(TeamViewHolder holder, int position) {
        if (mDataset == null || mDataset.size() - 1 < position || mDataset.get(position) == null) {
            return;
        }

        final NhlTeam team = mDataset.get(position);

        holder.itemView.setId(position);
        holder.slug = team.getSlug();
        holder.name.setText(team.getTeamShortName());
        holder.logo.setImageResource(team.getTeamLogo());
    }

    @Override
    public int getItemCount() {
        return (mDataset == null) ? 0 : mDataset.size();
    }

    public void swapDataset(List<NhlTeam> data) {
        this.mDataset = data;
        notifyDataSetChanged();
    }

    /**
     * Used to restoreSavedInstance, of previously selected team.
     * Item is not clickable (already clicked)
     *
     * @param singleItem
     */
    public void swapDataset(NhlTeam singleItem) {
        // Skip adding the OnClickListener
        mEnableOnItemClick = false;

        // Create a single-item array the swapDataset
        final List<NhlTeam> list = new ArrayList<>();
        list.add(singleItem);
        swapDataset(list);
    }

    public void cropDataset(int position) {
        final int size = getItemCount();
        if (size > 2 && size > position) {
            if (position > 0) {
                Collections.swap(mDataset, position, 0);
                notifyItemMoved(position, 0);
            }


            for (int i = size - 1; i > 0; i--) {
                mDataset.remove(i);
            }
            notifyItemRangeRemoved(1, size);
        }
    }


    class TeamViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private String slug;
        private TextView name;
        private ImageView logo;
        private OnTeamSelectedListener listener;

        public TeamViewHolder(View itemView, OnTeamSelectedListener listener) {
            super(itemView);

            this.listener = listener;
            this.name = (TextView) itemView.findViewById(R.id.name);
            this.logo = (ImageView) itemView.findViewById(R.id.logo);

            if (mEnableOnItemClick) {
                itemView.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            // First, disable Item to avoid duplicate clicking
            itemView.setOnClickListener(null);
            itemView.setEnabled(false);

            // Remove other items
            cropDataset(itemView.getId());

            if (listener != null) {
                // Notify listener
                listener.onTeamSelected(slug);
            }
        }
    }

    public interface OnTeamSelectedListener {
        void onTeamSelected(String team);
    }
}
