/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.ui.fragment.CurrentGamesListFragment;
import ca.mudar.huegoallight.ui.fragment.UpcomingGamesListFragment;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class MainTabsAdapter extends FragmentPagerAdapter implements
        Const.MainTabs {
    private static final String TAG = makeLogTag("MainTabsAdapter");

    private final String[] titles;

    public MainTabsAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.titles = context.getResources().getStringArray(R.array.main_tabs);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;

        switch (position) {
            case CURRENT:
                fragment = CurrentGamesListFragment.newInstance();
                break;
            case UPCOMING:
                fragment = UpcomingGamesListFragment.newInstance();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return _COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

}
