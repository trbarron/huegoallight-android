/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.listener;


import android.content.Context;
import android.text.TextUtils;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.PHMessageType;
import com.philips.lighting.hue.sdk.PHSDKListener;
import com.philips.lighting.hue.sdk.exception.PHHueException;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeConfiguration;
import com.philips.lighting.model.PHHueParsingError;
import com.philips.lighting.model.PHLight;
import com.squareup.otto.Bus;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.utils.HueBridgeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.LOGW;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class HueSDKListener implements PHSDKListener {
    private static final String TAG = makeLogTag("HueSDKListener");
    private static final long FAST_HEART_BEAT = 1000;

    private final Context context;
    private final PHHueSDK hueSDK;
    private Bus bus;
    private Boolean mSingleBridgeAutoConnect;
    private long mHeartBeatInterval;

    public HueSDKListener(Context context) {
        this.context = context;
        this.hueSDK = PHHueSDK.getInstance();
        this.bus = HueGoalApp.getSyncBus();
        this.mSingleBridgeAutoConnect = true;
        this.mHeartBeatInterval = PHHueSDK.HB_INTERVAL;
    }

    @Override
    public void onCacheUpdated(List<Integer> list, PHBridge bridge) {
//        LOGV(TAG, "onCacheUpdated");

        bus.post(new SyncBusEvents.HueCacheUpdated());
    }

    @Override
    public void onBridgeConnected(PHBridge bridge, String username) {
        LOGV(TAG, "onBridgeConnected");
        hueSDK.stopPushlinkAuthentication();

        final PHBridgeConfiguration conf = bridge.getResourceCache().getBridgeConfiguration();

        hueSDK.setSelectedBridge(bridge);
        hueSDK.enableHeartbeat(bridge, mHeartBeatInterval);
        hueSDK.getLastHeartbeat().put(conf.getIpAddress(),
                System.currentTimeMillis());

        final HuePrefs prefs = HuePrefs.getInstance(context);
        prefs.setBridgeConfiguration(username, conf.getIpAddress(), conf.getBridgeID());

        final String normalizedBridgeId = HueBridgeUtils.getNormalizedBridgeId(conf.getBridgeID());
        if (prefs.isKnownBridge(normalizedBridgeId)) {
            prefs.restoreBridgeSelectedLights(normalizedBridgeId);
        } else {
            // Select all lights for a new bridge
            final List<String> lights = new ArrayList<>();
            for (PHLight light : bridge.getResourceCache().getAllLights()) {
                lights.add(light.getUniqueId());
            }
            prefs.setSelectedLights(lights);
        }

        bus.post(new SyncBusEvents.BridgeConnected());
    }

    @Override
    public void onAuthenticationRequired(PHAccessPoint accessPoint) {
        LOGV(TAG, "onAuthenticationRequired");

        hueSDK.startPushlinkAuthentication(accessPoint);

        bus.post(new SyncBusEvents.BridgeAuthRequired());
    }

    @Override
    public void onAccessPointsFound(List<PHAccessPoint> list) {
        LOGV(TAG, "onAccessPointsFound");

        if (list != null && list.size() > 0) {
//            list = HueBridgeUtils.addTestAccessPoints(hueSDK.getAccessPointsFound());

            hueSDK.getAccessPointsFound().clear();
            hueSDK.getAccessPointsFound().addAll(list);

            if (list.size() == 1) {
                if (mSingleBridgeAutoConnect) {
                    onAccessPointSelected(list.get(0));
                }
                bus.post(new SyncBusEvents.AccessPointFound(mSingleBridgeAutoConnect));
            } else {
                bus.post(new SyncBusEvents.MultiAccessPointsFound());
            }
        }
    }

    public void onAccessPointSelected(PHAccessPoint accessPoint) {
        LOGV(TAG, "onAccessPointSelected");

        if (accessPoint == null) {
            return;
        }
        // Verify if previously connected
        final PHBridge connectedBridge = hueSDK.getSelectedBridge();
        if (connectedBridge != null) {
            String connectedIP = connectedBridge.getResourceCache().getBridgeConfiguration().getIpAddress();
            if (connectedIP != null) {   // We are already connected here
                hueSDK.disableHeartbeat(connectedBridge);
                hueSDK.disconnect(connectedBridge);
            }
        }

        if (TextUtils.isEmpty(accessPoint.getUsername())) {
            if (HueBridgeUtils.isLastAccessPoint(context, accessPoint)) {
                accessPoint.setUsername(HuePrefs.getInstance(context).getUsername());
            } else {
                accessPoint.setUsername(HueBridgeUtils.getRandomUsername());
            }
        }

        // Connect to new access point
        try {
            hueSDK.connect(accessPoint);
        } catch (PHHueException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onError(int code, final String message) {
        LOGW(TAG, "onError "
                + String.format("code = %s, message = %s", code, message));

        switch (code) {
            case PHMessageType.PUSHLINK_BUTTON_NOT_PRESSED:
                bus.post(new SyncBusEvents.PushlinkWarn());
                break;
//            case PHHueError.NO_CONNECTION:
//                break;
//            case PHHueError.AUTHENTICATION_FAILED:
//                break;
//            case PHMessageType.PUSHLINK_AUTHENTICATION_FAILED:
//                break;
//            case PHHueError.BRIDGE_NOT_RESPONDING:
//                break;
//            case PHMessageType.BRIDGE_NOT_FOUND:
//                break;
            default:
                bus.post(new SyncBusEvents.BridgeError(code, message));
                break;
        }
    }

    @Override
    public void onConnectionResumed(PHBridge bridge) {
//        LOGV(TAG, "onConnectionResumed");
        hueSDK.stopPushlinkAuthentication();

        hueSDK.getLastHeartbeat().put(
                bridge.getResourceCache().getBridgeConfiguration().getIpAddress(),
                System.currentTimeMillis());

        final int size = hueSDK.getDisconnectedAccessPoint().size();
        for (int i = 0; i < size; i++) {
            if (hueSDK.getDisconnectedAccessPoint().get(i).getIpAddress().equals(
                    bridge.getResourceCache().getBridgeConfiguration().getIpAddress())) {
                hueSDK.getDisconnectedAccessPoint().remove(i);
            }
        }

        bus.post(new SyncBusEvents.BridgeConnectionResumed());
    }

    @Override
    public void onConnectionLost(PHAccessPoint accessPoint) {
        LOGV(TAG, "onConnectionLost");

        if (!hueSDK.getDisconnectedAccessPoint().contains(accessPoint)) {
            hueSDK.getDisconnectedAccessPoint().add(accessPoint);
        }

        bus.post(new SyncBusEvents.BridgeConnectionLost());
    }

    @Override
    public void onParsingErrors(List<PHHueParsingError> list) {
        LOGW(TAG, "onParsingErrors");

        for (PHHueParsingError parsingError : list) {
            LOGV(TAG, "ParsingError : " + parsingError.getMessage());
        }
    }

    public void setSingleBridgeAutoConnect(boolean autoConnect) {
        this.mSingleBridgeAutoConnect = autoConnect;
    }

    /**
     * Set fast hearbeat interval. Used by MessagingListenerService to force cache updates.
     */
    public void setFastHeartBeatInterval() {
        this.mHeartBeatInterval = FAST_HEART_BEAT;
    }
}
