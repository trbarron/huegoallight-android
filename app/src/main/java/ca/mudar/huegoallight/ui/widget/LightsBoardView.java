/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.philips.lighting.model.PHLight;
import com.woxthebox.draglistview.BoardView;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.Const.LightsColumns;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.model.LightsColumn;
import ca.mudar.huegoallight.ui.adapter.LightsDragItemAdapter;
import ca.mudar.huegoallight.ui.view.LightDragView;
import uk.co.samuelwall.materialtaptargetprompt.MaterialTapTargetPrompt;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class LightsBoardView extends BoardView {
    private static final String TAG = makeLogTag("LightsBoardView");

    private final int horizontalPadding;
    private final int verticalPadding;
    private final String titlePrimary;
    private final String titleSecondary;
    private final String titleRed;
    private final String titleUnused;

    public LightsBoardView(Context context) {
        this(context, null);
    }

    public LightsBoardView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.horizontalScrollViewStyle);
    }

    public LightsBoardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        final Resources res = getResources();
        this.horizontalPadding = res.getDimensionPixelSize(R.dimen.activity_horizontal_margin);
        this.verticalPadding = res.getDimensionPixelSize(R.dimen.activity_vertical_margin);
        this.titlePrimary = res.getString(R.string.lights_column_primary);
        this.titleSecondary = res.getString(R.string.lights_column_secondary);
        this.titleRed = res.getString(R.string.lights_column_red);
        this.titleUnused = res.getString(R.string.lights_column_unused);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        // BoardView config
        setSnapToColumnWhenDragging(true);
        setDragEnabled(true);
        setSnapDragItemToTouch(true);
        setSnapToColumnsWhenScrolling(false);
        setColumnWidth(getBestColumnWidth(getResources()));
        setCustomDragItem(new LightDragView(getContext(), R.layout.item_lights_grid));
    }

    public void addLightsColumnList(@LightsColumn int column, List<PHLight> lights) {
        final LightsDragItemAdapter adapter = new LightsDragItemAdapter(getContext());
        adapter.setItemList(lights != null ? lights : new ArrayList<PHLight>());

        // Call parent to add lights to the board
        final RecyclerView recyclerView = addColumnList(adapter, createHeaderView(column), false);

        styleRecyclerView(column, recyclerView);
    }

    public List<PHLight> getColumnLights(@LightsColumn int column) {
        final LightsDragItemAdapter adapter = (LightsDragItemAdapter) getRecyclerView(column).getAdapter();

        return (adapter != null) ? adapter.getItemList() : null;
    }

    public void showDiscovery(Activity activity) {
        final View firstChild = getRecyclerView(LightsColumns.PRIMARY_COLOR).getLayoutManager().getChildAt(0);
        if (firstChild != null) {
            final View target = firstChild.findViewById(R.id.drag_handle);
            new MaterialTapTargetPrompt.Builder(activity)
                    .setTarget(target)
                    .setPrimaryText(R.string.lights_discovery_title)
                    .setSecondaryText(R.string.lights_discovery_summary)
                    .setBackgroundColourFromRes(R.color.color_primary)
                    .setIcon(R.drawable.ic_drag_handle)
                    .setIconDrawableColourFilterFromRes(R.color.color_primary_dark)
                    .setIconDrawableTintMode(PorterDuff.Mode.SRC_ATOP)
                    .show();
        }
    }

    /**
     * Scroll to the column with most lights, ignoring the unused-lights column.
     */
    public void scrollToInitialColumn() {
        final int[] columns = {LightsColumns.PRIMARY_COLOR, LightsColumns.SECONDARY_COLOR,
                LightsColumns.RED_LIGHT};

        int maxLights = 0;
        int selectedColumn = LightsColumns.UNUSED_LIGHTS;
        for (int column : columns) {
            final List<PHLight> lights = getColumnLights(column);
            if (lights != null && lights.size() > maxLights) {
                maxLights = lights.size();
                selectedColumn = column;
            }
        }
        if (maxLights > 0) {
            // No need to scroll if all 3 columns are empty
            scrollToColumn(selectedColumn, false);
        }
    }

    private void styleRecyclerView(@LightsColumn int column, RecyclerView recyclerView) {
        if (column != LightsColumns.UNUSED_LIGHTS) {
            // Set left dotted border to all but first column
            recyclerView.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.column_left_border));
        }

        recyclerView.setClipToPadding(false);
        recyclerView.setPadding(horizontalPadding, verticalPadding, horizontalPadding, verticalPadding);
    }

    private static int getBestColumnWidth(Resources res) {
        final int defaultWidth = res.getDimensionPixelSize(R.dimen.board_column_width);
        final int optimalWidth = res.getDisplayMetrics().widthPixels / LightsColumns._COUNT;

        return Math.max(defaultWidth, optimalWidth);
    }

    private View createHeaderView(@LightsColumn int column) {
        final View header = View.inflate(getContext(), R.layout.header_lights_grid, null);
        final TextView vTitle = (TextView) header.findViewById(R.id.title);

        switch (column) {
            case LightsColumns.PRIMARY_COLOR:
                vTitle.setText(titlePrimary);
                break;
            case LightsColumns.SECONDARY_COLOR:
                vTitle.setText(titleSecondary);
                break;
            case LightsColumns.RED_LIGHT:
                vTitle.setText(titleRed);
                vTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.color_primary_dark));
                break;
            case LightsColumns.UNUSED_LIGHTS:
                vTitle.setText(titleUnused);
                break;
        }
        return header;
    }
}
