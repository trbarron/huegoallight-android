/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.philips.lighting.hue.sdk.PHHueSDK;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.service.ScheduleUpdateService;
import ca.mudar.huegoallight.ui.adapter.MainTabsAdapter;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.ui.widget.AutohideFabBehavior;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.GcmUtils;
import ca.mudar.huegoallight.utils.HueBlinkHelper;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.ImageUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class MainActivity extends AppCompatActivity implements
        SyncBusListener {
    private static final String TAG = makeLogTag("MainActivity");

    private PHHueSDK hueSDK;
    private HueSDKListener mHueSDKListener;
    private FloatingActionButton vFab;
    private AutohideFabBehavior mPagerBehavior;

    public static Intent newIntent(Context context, boolean isOnboarding, int nbAccessPoints) {
        final Intent intent = new Intent(context, MainActivity.class);

        final Bundle extras = new Bundle();
        extras.putBoolean(Const.BundleKeys.IS_ONBOARDING, isOnboarding);
        extras.putInt(Const.BundleKeys.NB_ACCESS_POINTS, nbAccessPoints);
        intent.putExtras(extras);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (showOnboardingIfNecessary(getIntent())) {
            finish();
            // Avoid duplicate calls to hueSdk
            return;
        }

        setContentView(R.layout.activity_main);

        vFab = (FloatingActionButton) findViewById(R.id.fab);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setupViewPager();
        setupHueBridge();
        setupFab();

        // Update season schedule, if necessary
        startService(ScheduleUpdateService.newIntent(this));

        checkQuietMode();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (GcmUtils.checkPlayServices(this)) {
            // Avoid displaying the PlayServices dialog followed by the PushLink activity
            registerSyncBus();
        }

        // TODO reconnect to hue if not connected

        if (hueSDK.getSelectedBridge() != null) {
            onBridgeConnected(null);
        }

        ConnectionUtils.checkConnection(this, findViewById(R.id.main_content));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        if (hueSDK.getSelectedBridge() == null) {
            final MenuItem hueSetup = menu.findItem(R.id.action_hue_setup);
            // Tint the icon only if visible
            tintMenuIconCompat(hueSetup);
            // Hue is not available, disable Settings and show HueSetup
            menu.findItem(R.id.action_settings).setEnabled(false);
            hueSetup.setVisible(true);
        } else {
            // Hue is ready, enable Settings and hide HueSetup
            menu.findItem(R.id.action_settings).setEnabled(true);
            menu.findItem(R.id.action_hue_setup).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(SettingsActivity.newIntent(this));
            return true;
        } else if (id == R.id.action_hue_setup) {
            startActivityForResult(HueSetupActivity.newBridgeSelectorIntent(this), Const.RequestCodes.HUE_SETUP);
            return true;
        } else if (id == R.id.action_about) {
            startActivity(AboutActivity.newIntent(this));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Const.RequestCodes.HUE_SETUP) {
            if (resultCode != Activity.RESULT_OK) {
//                LOGE(TAG, "Push-link error");
                finish();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterSyncBus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        HueBridgeUtils.disconnectBridgeIfNecessary(hueSDK);
    }

    private void setupViewPager() {
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(new MainTabsAdapter(this, getSupportFragmentManager()));

        // Get scroll behavior
        mPagerBehavior = (AutohideFabBehavior) ((CoordinatorLayout.LayoutParams) viewPager.getLayoutParams()).getBehavior();

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);
                // No need to force-show FAB for upcoming games
                if ((tab.getPosition() == Const.MainTabs.CURRENT) && shouldShowFab()) {
                    vFab.show();
                }
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                super.onTabReselected(tab);
                if (shouldShowFab()) {
                    vFab.show();
                }
            }
        });
    }


    @Subscribe
    public void onBridgeAuthRequired(SyncBusEvents.BridgeAuthRequired event) {
        startActivityForResult(HueSetupActivity.newAuthenticationIntent(this), Const.RequestCodes.HUE_SETUP);
    }


    @Subscribe
    public void onAccessPointFound(SyncBusEvents.AccessPointFound event) {
        if (!event.isAutoConnect()) {
            startActivityForResult(HueSetupActivity.newBridgeSelectorIntent(this), Const.RequestCodes.HUE_SETUP);
        } else {
            LOGV(TAG, "onAccessPointSelected, skipped");
        }
    }

    @Subscribe
    public void onMultiAccessPointsFound(SyncBusEvents.MultiAccessPointsFound event) {
        startActivityForResult(HueSetupActivity.newBridgeSelectorIntent(this), Const.RequestCodes.HUE_SETUP);
    }

    @Subscribe
    public void onBridgeConnected(SyncBusEvents.BridgeConnected event) {
        LOGV(TAG, "onBridgeConnected");

        toggleHueStatus(true);
    }

    @Subscribe
    public void onBridgeConnectionResumed(SyncBusEvents.BridgeConnectionResumed event) {
        LOGV(TAG, "onBridgeConnectionResumed");

        toggleHueStatus(true);
    }

    @Subscribe
    public void onBridgeError(SyncBusEvents.BridgeError error) {
        startActivityForResult(HueSetupActivity.newErrorIntent(this, error), Const.RequestCodes.HUE_SETUP);
    }

    @Subscribe
    public void onBlinkStart(SyncBusEvents.LightBlinkStarted event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vFab.setEnabled(false);
            }
        });
    }

    @Subscribe
    public void onBlinkStopped(SyncBusEvents.LightBlinkStopped event) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vFab.setEnabled(true);
            }
        });
    }

    @Subscribe
    public void onLightSelectionError(SyncBusEvents.LightsSelectionError error) {
        toggleHueStatus(false);

        Snackbar.make(findViewById(R.id.main_content),
                R.string.snackbar_hue_lights_selection_error,
                Snackbar.LENGTH_LONG
        ).setAction(R.string.snackbar_btn_settings, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(LightsBoardActivity.newIntent(MainActivity.this));
            }
        }).show();
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            // Happens when checkPlayServices() returns false,
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Shows Onboarding screens: TeamsList (#1) or Push-Link/BridgeFinder (#2)
     *
     * @param intent
     * @return true if MainActivity should call finish()
     */
    private boolean showOnboardingIfNecessary(Intent intent) {
        if (HuePrefs.getInstance(getApplicationContext()).isOnboarding()) {
            // Show Teams list
            startActivity(OnboardingActivity.newIntent(this));

            // This tells MainActivity to finish() itself
            return true;
        } else {
            if (intent.getBooleanExtra(Const.BundleKeys.IS_ONBOARDING, false)) {
                final int nbAccessPointsFound = intent.getIntExtra(Const.BundleKeys.NB_ACCESS_POINTS, 0);
                startActivityForResult(HueSetupActivity.newOnboardingIntent(this, nbAccessPointsFound), Const.RequestCodes.HUE_SETUP);
                // Clear value to avoid restarting HueSetup on rotation
                intent.removeExtra(Const.BundleKeys.IS_ONBOARDING);
            }
        }

        return false;
    }

    private void setupHueBridge() {
        hueSDK = PHHueSDK.getInstance();
        mHueSDKListener = HueBridgeUtils.initializeHueBridge(new ContextWrapper(this), hueSDK);
    }

    private void setupFab() {
        toggleHueStatus(false);
        final List<Integer> hues = new ArrayList<>();
        hues.add(Const.DEFAULT_RGB_COLOR);
        hues.add(Const.ALTERNATE_RGB_COLOR);

        vFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vFab.setEnabled(false);
                new HueBlinkHelper(getApplicationContext(), hues).startBlinking();
            }
        });
    }

    /**
     * Show FAB only if Hue is connected
     *
     * @return true when FAB should be displayed
     */
    private boolean shouldShowFab() {
        return !vFab.isShown() && vFab.isEnabled() && (hueSDK.getSelectedBridge() != null);
    }

    /**
     * Toggles FAB visibility and invalidates OptionsMenu, relies on Hue connection
     *
     * @param enabled
     */
    private void toggleHueStatus(final boolean enabled) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                supportInvalidateOptionsMenu();

                vFab.setEnabled(enabled);
                if (enabled) {
                    vFab.show();
                    if (mPagerBehavior != null) {
                        mPagerBehavior.setActive(true);
                    }
                } else {
                    vFab.hide();
                    if (mPagerBehavior != null) {
                        // When FAB is disabled, we also need to remove the Autohide layout behavior
                        mPagerBehavior.setActive(false);
                    }
                }
            }
        });
    }

    private void tintMenuIconCompat(MenuItem item) {
        if (!Const.SUPPORTS_LOLLIPOP) {
            item.setIcon(ImageUtils.tintDrawable(item.getIcon(), Color.WHITE));
        }
    }

    private void checkQuietMode() {
        if (HuePrefs.getInstance(getApplicationContext()).isCurrentlyQuietHours()) {
            Snackbar.make(findViewById(R.id.main_content),
                    R.string.snackbar_currently_quiet_hours,
                    Snackbar.LENGTH_LONG
            ).setAction(R.string.snackbar_btn_settings, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(SettingsQuietHoursActivity.newIntent(MainActivity.this));
                }
            }).show();
        }
    }
}
