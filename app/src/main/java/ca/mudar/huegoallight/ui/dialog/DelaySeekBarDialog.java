/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.dialog;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.BundleKeys;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.ui.view.RadialTimePickerView;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class DelaySeekBarDialog extends DialogFragment implements
        DialogInterface.OnClickListener {
    private static final String TAG = makeLogTag("DelaySeekBarDialog");

    private RadialTimePickerView vSeekBar;
    private TextView vHeader;
    private String mId;
    private int mValue;

    public static DelaySeekBarDialog newInstance(String id, int value) {
        final DelaySeekBarDialog dialog = new DelaySeekBarDialog();

        final Bundle args = new Bundle();
        args.putString(BundleKeys.GAME_ID, id);
        args.putInt(BundleKeys.SECONDS, value);
        dialog.setArguments(args);

        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setRetainInstance(true);

        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_delay_seekbar, null);

        // load mId & mValue
        loadInitialValues(savedInstanceState != null ? savedInstanceState : getArguments());

        vHeader = (TextView) view.findViewById(R.id.header);
        vHeader.setText(getString(R.string.game_delay_seconds, mValue));

        setupSeekBar(view, mValue);

        final AlertDialog dialog = new AlertDialog.Builder(getActivity(), R.style.AppTheme_AlertDialog)
                .setTitle(null)
                .setView(view)
                .setPositiveButton(R.string.btn_ok, this)
                .setNegativeButton(R.string.btn_cancel, null) // no listener
                .setNeutralButton(R.string.btn_help, null) // no listener
                .create();

        setupNeutralButtonListener(dialog);

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialog, int which) {
        if (which == Dialog.BUTTON_POSITIVE) {
            final Fragment target = getTargetFragment();
            if (target instanceof SeekBarCallback) {
                ((SeekBarCallback) target).onSeekBarValueChange(mId, mValue);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(BundleKeys.GAME_ID, mId);
        outState.putInt(BundleKeys.SECONDS, mValue);
    }

    @Override
    public void onDestroyView() {
        if (getDialog() != null && getRetainInstance()) {
            getDialog().setDismissMessage(null);
        }

        super.onDestroyView();
    }

    private void loadInitialValues(Bundle bundle) {
        if (bundle != null) {
            mId = bundle.getString(BundleKeys.GAME_ID, null);
            mValue = bundle.getInt(BundleKeys.SECONDS, 0);
        } else {
            mId = null;
            mValue = 0;
        }
    }

    private void setupSeekBar(View view, int minutes) {
        vSeekBar = (RadialTimePickerView) view.findViewById(R.id.picker);
        vSeekBar.setCurrentItemShowing(RadialTimePickerView.MINUTES, false);
        vSeekBar.setOnValueSelectedListener(new RadialTimePickerView.OnValueSelectedListener() {
            @Override
            public void onValueSelected(@RadialTimePickerView.PickerType int pickerType, int newValue, boolean autoAdvance) {
                mValue = newValue;
                vHeader.setText(getString(R.string.game_delay_seconds, mValue));
            }
        });

        vSeekBar.setCurrentMinute(minutes);
    }

    /**
     * Handle the neutral button callback manually, to avoid dismissing the dialog
     *
     * @param dialog
     */
    private void setupNeutralButtonListener(AlertDialog dialog) {
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_NEUTRAL);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        final GameDelayHelpFragment bottomSheet = GameDelayHelpFragment.newInstance();
                        bottomSheet.show(getActivity().getSupportFragmentManager(),
                                Const.FragmentTags.GAME_DELAY_HELP);
                    }
                });
            }
        });
    }

    public interface SeekBarCallback {
        void showSeekBarDialog(String id, int value);

        void onSeekBarValueChange(String id, int value);
    }
}
