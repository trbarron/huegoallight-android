/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.List;

import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.data.Queries;
import ca.mudar.huegoallight.provider.EventsContract;
import ca.mudar.huegoallight.ui.adapter.UpcomingGamesAdapter;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;


public class UpcomingGamesListFragment extends Fragment implements
        SyncBusListener,
        LoaderManager.LoaderCallbacks<Cursor> {
    private static final String TAG = makeLogTag("UpcomingGamesListFragment");

    private UpcomingGamesAdapter mAdapter;
    private TextView vEmpty;
    private View vProgress;

    public static UpcomingGamesListFragment newInstance() {
        return new UpcomingGamesListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerSyncBus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_upcoming_games_list, container, false);

        vProgress = view.findViewById(R.id.progress);
        vEmpty = (TextView) view.findViewById(R.id.empty_games);

        setupRecyclerView((RecyclerView) view.findViewById(R.id.recycler));

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(Queries.UpcomingGamesQuery._TOKEN, null, this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterSyncBus();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == Queries.UpcomingGamesQuery._TOKEN) {
            final List<String> followedTeams = HuePrefs.getInstance(getContext()).getFollowedNhlTeams();

            return new CursorLoader(getActivity().getBaseContext(),
                    EventsContract.Games.CONTENT_URI_UPCOMING,
                    Queries.UpcomingGamesQuery.PROJECTION,
                    EventsContract.Games.getSelectionByTeams(followedTeams, true),
                    null,
                    EventsContract.Games.DEFAULT_SORT);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == Queries.UpcomingGamesQuery._TOKEN) {
            vProgress.setVisibility(View.GONE);
            mAdapter.swapDataset(data);

            final boolean isEmpty = (data == null) || !data.moveToFirst();
            vEmpty.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Subscribe
    public void onTeamsUpdated(SyncBusEvents.GcmTopicsUpdated event) {
        try {
            getLoaderManager().restartLoader(Queries.UpcomingGamesQuery._TOKEN, null, UpcomingGamesListFragment.this);
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }

//        if (getActivity() != null) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    getLoaderManager().restartLoader(Queries.UpcomingGamesQuery._TOKEN, null, UpcomingGamesListFragment.this);
//                }
//            });
//        }
    }

    private void setupRecyclerView(RecyclerView recycler) {
        recycler.setLayoutManager(new LinearLayoutManager(recycler.getContext()));

        mAdapter = new UpcomingGamesAdapter(getActivity(), R.layout.item_upcoming_game);
        recycler.setAdapter(mAdapter);
    }

}

