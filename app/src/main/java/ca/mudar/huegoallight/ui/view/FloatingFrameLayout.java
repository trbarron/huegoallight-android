/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import ca.mudar.huegoallight.R;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

/**
 * Ref: Daniel Sandler https://code.google.com/archive/p/android-daydream-samples/
 */
public class FloatingFrameLayout extends FrameLayout {
    private static final String TAG = makeLogTag("FloatingFrameLayout");

    public FloatingFrameLayout(Context context) {
        this(context, null);
    }

    public FloatingFrameLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FloatingFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();

        jump();
    }

    public void jump() {
        final View parent = (View) getParent();
        if (parent == null) {
            return;
        }

        // Set random position using the view's and its parent's dimensions
        setX((float) Math.random() * (parent.getMeasuredWidth() - getMeasuredWidth()));
        setY((float) Math.random() * (parent.getMeasuredHeight() - getMeasuredHeight()));

        // Fade in/out
        setAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.fade_in_out));
    }
}
