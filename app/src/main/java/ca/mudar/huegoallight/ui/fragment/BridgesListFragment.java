/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.philips.lighting.hue.sdk.PHHueSDK;
import com.squareup.otto.Subscribe;

import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.ui.adapter.BridgesAdapter;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.LOGE;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class BridgesListFragment extends Fragment implements
        SyncBusListener {
    private static final String TAG = makeLogTag("BridgesListFragment");

    private BridgesAdapter.OnBridgeSelectedListener mListener;
    private BridgesAdapter mAdapter;
    private View vProgress;

    public static BridgesListFragment newInstance() {
        return new BridgesListFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (BridgesAdapter.OnBridgeSelectedListener) context;
        } catch (ClassCastException e) {
            LOGE(TAG, "Activity must implement OnBridgeSelectedListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_bridges_list, container, false);

        vProgress = view.findViewById(R.id.progress);
        setupRecyclerView((RecyclerView) view.findViewById(R.id.recycler));

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        registerSyncBus();
    }

    @Override
    public void onPause() {
        super.onPause();

        unregisterSyncBus();
    }

    private void setupRecyclerView(RecyclerView recycler) {
        recycler.setLayoutManager(new LinearLayoutManager(recycler.getContext()));

        mAdapter = new BridgesAdapter(getActivity(), R.layout.item_bridge, mListener);
        recycler.setAdapter(mAdapter);
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Subscribe
    public void onAccessPointFound(SyncBusEvents.AccessPointFound event) {
        swapAdapterDataset();
    }

    @Subscribe
    public void onMultiAccessPointsFound(SyncBusEvents.MultiAccessPointsFound event) {
        swapAdapterDataset();
    }

    private void swapAdapterDataset() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                vProgress.setVisibility(View.GONE);
                mAdapter.swapDataset(PHHueSDK.getInstance().getAccessPointsFound());
//                mAdapter.swapDataset(HueBridgeUtils.addTestAccessPoints(PHHueSDK.getInstance().getAccessPointsFound()));
            }
        });
    }
}
