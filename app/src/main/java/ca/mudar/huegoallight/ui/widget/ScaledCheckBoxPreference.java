/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.preference.CheckBoxPreference;
import android.util.AttributeSet;

import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.utils.ImageUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;


public class ScaledCheckBoxPreference extends CheckBoxPreference {
    private static final String TAG = makeLogTag("ScaledCheckBoxPreference");

    public ScaledCheckBoxPreference(Context context) {
        this(context, null);
    }

    public ScaledCheckBoxPreference(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.checkBoxPreferenceStyle);
    }

    public ScaledCheckBoxPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        if (attrs != null) {
            final TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ScaledCheckBoxPreference, defStyleAttr, 0);

            if (ta != null) {
                final int defaultSize = getContext().getResources().getDimensionPixelSize(R.dimen.prefs_icon_size);
                final int size = ta.getDimensionPixelSize(R.styleable.ScaledCheckBoxPreference_scaledCbPrefIconSize, defaultSize);
                final int iconResId = ta.getResourceId(R.styleable.ScaledCheckBoxPreference_scaledCbPrefIcon, 0);
                ta.recycle();

                if (iconResId != 0) {
                    setIcon(ImageUtils.getScaledDrawable(context, iconResId, size, size));
                }
            }
        }
    }
}
