/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;


import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageButton;
import android.widget.TextView;

import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.DatabaseHelper;
import ca.mudar.huegoallight.data.Queries;
import ca.mudar.huegoallight.ui.dialog.DelaySeekBarDialog;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class CurrentGamesAdapter extends UpcomingGamesAdapter {
    private static final String TAG = makeLogTag("CurrentGamesAdapter");
    private static final int TYPE_UPCOMING = 0;
    private static final int TYPE_CURRENT = 1;

    private final int itemLayoutCurrent;
    private final DelaySeekBarDialog.SeekBarCallback listener;
    private long mCurrentTime;

    public CurrentGamesAdapter(Context context, @LayoutRes int itemLayoutCurrent, @LayoutRes int itemLayoutUpcoming,
                               DelaySeekBarDialog.SeekBarCallback callback) {
        super(context, itemLayoutUpcoming);

        this.itemLayoutCurrent = itemLayoutCurrent;
        this.mCurrentTime = System.currentTimeMillis();
        this.listener = callback;
    }

    @Override
    public int getItemViewType(int position) {
        if (getCursor() == null || !getCursor().moveToPosition(position)) {
            return TYPE_UPCOMING;
        }

        final long startsAt = getCursor().getLong(Queries.CurrentGamesQuery.STARTS_AT);
        if (Long.compare(startsAt, mCurrentTime) < 0) {
            return TYPE_CURRENT;
        }

        return TYPE_UPCOMING;
    }

    @Override
    public GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (TYPE_CURRENT == viewType) {
            final View v = LayoutInflater.from(parent.getContext()).inflate(itemLayoutCurrent, parent, false);
            return new CurrentGameViewHolder(v, listener);
        }

        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void swapDataset(Cursor cursor) {
        this.mCurrentTime = System.currentTimeMillis();

        super.swapDataset(cursor);
    }

    protected static class CurrentGameViewHolder extends GameViewHolder implements
            View.OnClickListener {
        private TextView vAwayScore;
        private TextView vHomeScore;
        private TextView vAwayTeam;
        private TextView vHomeTeam;
        private ImageButton vOverflow;
        private Uri mGameUri;
        private DelaySeekBarDialog.SeekBarCallback listener;
        private String mGameId;
        private int mDelay;
        private boolean mIsMuted;

        CurrentGameViewHolder(View view, DelaySeekBarDialog.SeekBarCallback callback) {
            super(view);

            this.vAwayScore = (TextView) itemView.findViewById(R.id.away_score);
            this.vAwayTeam = (TextView) itemView.findViewById(R.id.away_team);
            this.vHomeScore = (TextView) itemView.findViewById(R.id.home_score);
            this.vHomeTeam = (TextView) itemView.findViewById(R.id.home_team);
            this.vOverflow = (ImageButton) itemView.findViewById(R.id.overflow_menu);
            this.listener = callback;

            itemView.setOnClickListener(this);
            vOverflow.setOnClickListener(this);
        }

        @Override
        public void fillGameInfo(Context context, Cursor data) {
            super.fillGameInfo(context, data);

            final Resources res = context.getResources();
            final int awayScore = data.getInt(Queries.CurrentGamesQuery.AWAY_TEAM_SCORE);
            final int homeScore = data.getInt(Queries.CurrentGamesQuery.HOME_TEAM_SCORE);
            final String url = data.getString(Queries.CurrentGamesQuery.URL);
            mGameId = data.getString(Queries.CurrentGamesQuery.REMOTE_ID);
            mDelay = data.getInt(Queries.CurrentGamesQuery.DELAY);
            mIsMuted = data.getInt(Queries.CurrentGamesQuery.IS_MUTED) == 1;

            vAwayScore.setText(String.valueOf(awayScore));
            vAwayTeam.setText(res.getString(getAwayTeam().getTeamName()));

            vHomeScore.setText(String.valueOf(homeScore));
            vHomeTeam.setText(res.getString(getHomeTeam().getTeamName()));

            // Handle the GameCenter link
            mGameUri = URLUtil.isNetworkUrl(url) ? Uri.parse(url) : null;

            getFooterView().setVisibility(mIsMuted ? View.GONE : View.VISIBLE);

            // Highlight active overflow buttons
            if (mIsMuted || mDelay > 0) {
                vOverflow.setColorFilter(ContextCompat.getColor(context, R.color.overflow_highlight),
                        PorterDuff.Mode.SRC_ATOP);
            } else {
                vOverflow.setColorFilter(null);
            }
        }

        @Override
        protected void setTextColor(@ColorInt int color) {
            vAwayScore.setTextColor(color);
            vAwayTeam.setTextColor(color);
            vHomeScore.setTextColor(color);
            vHomeTeam.setTextColor(color);
        }

        /**
         * Implements View.OnClickListener
         *
         * @param v
         */
        @Override
        public void onClick(View v) {
            if (v.getId() == R.id.overflow_menu) {
                final ContextWrapper context = new ContextWrapper(itemView.getContext());
                context.setTheme(R.style.AppTheme_PopupMenu);

                final PopupMenu popup = new PopupMenu(context, vOverflow, Gravity.NO_GRAVITY);
                final Menu menu = popup.getMenu();
                popup.getMenuInflater().inflate(R.menu.popup_game_card, menu);

                menu.findItem(R.id.action_mute).setChecked(mIsMuted);
                menu.findItem(R.id.action_delay).setEnabled(!mIsMuted);

                if (mDelay > 0) {
                    menu.findItem(R.id.action_delay).setTitle(
                            context.getResources().getString(R.string.action_delay_value, mDelay));
                }

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        final int id = item.getItemId();
                        if (id == R.id.action_delay) {
                            listener.showSeekBarDialog(mGameId, mDelay);
                        } else if (id == R.id.action_mute) {
                            DatabaseHelper.updateGameMute(context.getApplicationContext(),
                                    mGameId, !mIsMuted);
                        }
                        return false;
                    }
                });
                popup.show();
            } else if (mGameUri != null) {
                v.getContext().startActivity(new Intent(Intent.ACTION_VIEW, mGameUri));
            }
        }
    }
}
