/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.mikepenz.aboutlibraries.LibsBuilder;

import ca.mudar.huegoallight.BuildConfig;
import ca.mudar.huegoallight.R;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class AboutActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = makeLogTag("AboutActivity");
    private static final String SEND_INTENT_TYPE = "text/plain";

    public static Intent newIntent(Context context) {
        return new Intent(context, AboutActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_about);

        // Setup toolbar
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.app_name);

        setupTextViews();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_about, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final int id = item.getItemId();

        if (id == R.id.action_rate) {
            showWebsite(R.string.url_playstore);
            return true;
        } else if (id == R.id.action_share) {
            onShareItemSelected();
            return true;
        } else if (id == R.id.action_eula) {
            startActivity(EulaActivity.newIntent(this));
            return true;
        } else if (id == R.id.action_about_libs) {
            onAboutLibsItemSelected();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.about_credits) {
            showWebsite(R.string.url_mudar_ca);
        } else if (v.getId() == R.id.about_source_code) {
            showWebsite(R.string.url_gitlab);
        } else if (v.getId() == R.id.about_logos) {
            showWebsite(R.string.url_freevector_co);
        }
    }

    /**
     * Add click listeners and show version number
     */
    private void setupTextViews() {
        ((TextView) findViewById(R.id.about_version)).setText(
                getString(R.string.about_version, BuildConfig.VERSION_NAME));
        findViewById(R.id.about_credits).setOnClickListener(this);
        findViewById(R.id.about_source_code).setOnClickListener(this);
        findViewById(R.id.about_logos).setOnClickListener(this);
    }

    /**
     * Native sharing
     */
    private void onShareItemSelected() {
        final Bundle extras = new Bundle();
        extras.putString(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_intent_title));
        extras.putString(Intent.EXTRA_TEXT, getResources().getString(R.string.url_playstore));

        final Intent sendIntent = new Intent();
        sendIntent.putExtras(extras);
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType(SEND_INTENT_TYPE);
        startActivity(sendIntent);
    }

    /**
     * Show the AboutLibraries acknowledgements activity
     */
    private void onAboutLibsItemSelected() {
        new LibsBuilder()
                .withActivityTitle(getString(R.string.activity_about_libs))
                .withActivityTheme(R.style.AppTheme_Toolbar)
                .withAutoDetect(false) // For Proguard
                .withFields(R.string.class.getFields()) // For Proguard
                .withLibraries(
                        "GooglePlayServices", "huesdk", "Otto"
                        // Added manually to avoid issues with Proguard
                        , "AboutLibraries", "Crashlytics", "gson", "OkHttp"
                        , "Retrofit", "appcompat_v7", "design", "recyclerview_v7"
                        , "materialtaptargetprompt", "draglistview"
                )
                .withExcludedLibraries(
                        "AndroidIconics", "fastadapter", "okio", "support_v4"
                )
                .start(this);
    }

    /**
     * Launch intent to view website
     *
     * @param website
     */
    private void showWebsite(@StringRes int website) {
        final Intent viewIntent = new Intent(Intent.ACTION_VIEW);
        viewIntent.setData(Uri.parse(getResources().getString(website)));
        startActivity(viewIntent);
    }
}
