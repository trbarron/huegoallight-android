/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.PrefsNames;
import ca.mudar.huegoallight.Const.PrefsValues;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.ui.fragment.SettingsQuietHoursFragment;

import static ca.mudar.huegoallight.Const.FragmentTags.SETTINGS_QUIET_HOURS;

public class SettingsQuietHoursActivity extends AppCompatActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener,
        CompoundButton.OnCheckedChangeListener {

    private SwitchCompat mSwitchPref;
    private SharedPreferences mSharedPrefs;

    public static Intent newIntent(Context context) {
        return new Intent(context, SettingsQuietHoursActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_quiet_hours_settings);

        mSwitchPref = (SwitchCompat) findViewById(R.id.master_switch);

        // Prefs listener
        mSharedPrefs = getSharedPreferences(Const.APP_PREFS_NAME, Context.MODE_PRIVATE);
        mSharedPrefs.registerOnSharedPreferenceChangeListener(this);

        setupSwitchPreference();

        if (savedInstanceState == null) {
            Fragment fragment = getFragmentManager().findFragmentByTag(SETTINGS_QUIET_HOURS);
            if (fragment == null) {
                fragment = SettingsQuietHoursFragment.newInstance();
            }

            getFragmentManager().beginTransaction()
                    .replace(R.id.content_frame, fragment, SETTINGS_QUIET_HOURS)
                    .commit();
        }
    }


    @Override
    protected void onDestroy() {
        if (mSharedPrefs != null) {
            mSharedPrefs.unregisterOnSharedPreferenceChangeListener(this);
        }

        super.onDestroy();
    }

    /**
     * Implements SharedPreferences.OnSharedPreferenceChangeListener
     *
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (PrefsNames.QUIET_HOURS_ENABLED.equals(key)) {
            final boolean isEnabled = sharedPreferences.getBoolean(PrefsNames.QUIET_HOURS_ENABLED, PrefsValues.DEFAULT_QUIET_HOURS_ENABLED);
            mSwitchPref.setChecked(isEnabled);
        }
    }

    /**
     * Implements CompoundButton.OnCheckedChangeListener
     *
     * @param buttonView
     * @param isChecked
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        toggleSwitchText(isChecked);

        mSharedPrefs.edit()
                .putBoolean(PrefsNames.QUIET_HOURS_ENABLED, isChecked)
                .apply();
    }

    private void setupSwitchPreference() {
        final boolean isEnabled = mSharedPrefs.getBoolean(PrefsNames.QUIET_HOURS_ENABLED, PrefsValues.DEFAULT_QUIET_HOURS_ENABLED);
        mSwitchPref.setChecked(isEnabled);
        toggleSwitchText(isEnabled);

        mSwitchPref.setOnCheckedChangeListener(this);
    }

    private void toggleSwitchText(boolean isChecked) {
        mSwitchPref.setText(isChecked ?
                R.string.prefs_summary_quiet_hours_on : R.string.prefs_summary_quiet_hours_off);
    }
}
