/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.DatabaseHelper;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.data.Queries;
import ca.mudar.huegoallight.io.ApiClient;
import ca.mudar.huegoallight.io.model.NhlGamesList;
import ca.mudar.huegoallight.provider.EventsContract;
import ca.mudar.huegoallight.ui.adapter.CurrentGamesAdapter;
import ca.mudar.huegoallight.ui.dialog.DelaySeekBarDialog;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.LogUtils;
import ca.mudar.huegoallight.utils.StringUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;


public class CurrentGamesListFragment extends Fragment implements
        SyncBusListener,
        LoaderManager.LoaderCallbacks<Cursor>,
        DelaySeekBarDialog.SeekBarCallback {
    private static final String TAG = makeLogTag("CurrentGamesListFragment");

    private CurrentGamesAdapter mAdapter;
    private SwipeRefreshLayout vSwipeRefresh;
    private View vProgress;
    private TextView vEmpty;
    private String mGamesIds;
    private long mLastUpdate;

    public static CurrentGamesListFragment newInstance() {
        return new CurrentGamesListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mLastUpdate = 0;

        registerSyncBus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_current_games_list, container, false);

        vSwipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh);
        vProgress = view.findViewById(R.id.progress);
        vEmpty = (TextView) view.findViewById(R.id.empty_games);
        setupRecyclerView((RecyclerView) view.findViewById(R.id.recycler));
        setupSwipeRefresh();

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getLoaderManager().initLoader(Queries.CurrentGamesQuery._TOKEN, null, this);
    }

    @Override
    public void onResume() {
        super.onResume();

        updateLiveScoresIfNecessary();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterSyncBus();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if (id == Queries.CurrentGamesQuery._TOKEN) {
            final List<String> followedTeams = HuePrefs.getInstance(getContext()).getFollowedNhlTeams();

            return new CursorLoader(getActivity().getBaseContext(),
                    EventsContract.Games.CONTENT_URI_CURRENT,
                    Queries.CurrentGamesQuery.PROJECTION,
                    EventsContract.Games.getSelectionByTeams(followedTeams),
                    null,
                    EventsContract.Games.DEFAULT_SORT);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if (loader.getId() == Queries.CurrentGamesQuery._TOKEN) {
            vProgress.setVisibility(View.GONE);
            mAdapter.swapDataset(data);

            if (data != null && data.moveToFirst()) {
                vEmpty.setVisibility(View.GONE);
                vSwipeRefresh.setEnabled(true);

                mGamesIds = getGamesIds(data);
                updateLiveScoresIfNecessary();
            } else {
                vEmpty.setVisibility(View.VISIBLE);
                vSwipeRefresh.setEnabled(false);
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements DelaySeekBarDialog.SeekBarChangeListener
     *
     * @param value
     */
    @Override
    public void showSeekBarDialog(String id, int value) {
        final DelaySeekBarDialog dialog = DelaySeekBarDialog.newInstance(id, value);
        dialog.setTargetFragment(this, Const.RequestCodes.DELAY_SEEKBAR);
        dialog.show(getActivity().getSupportFragmentManager(), Const.FragmentTags.DELAY_SEEKBAR);
    }

    /**
     * Implements DelaySeekBarDialog.SeekBarChangeListener
     *
     * @param value
     */
    @Override
    public void onSeekBarValueChange(String id, int value) {
        DatabaseHelper.updateGameDelay(getActivity().getApplicationContext(), id, value);
    }

    @Subscribe
    public void onTeamsUpdated(SyncBusEvents.GcmTopicsUpdated event) {
        try {
            getLoaderManager().restartLoader(Queries.CurrentGamesQuery._TOKEN, null, CurrentGamesListFragment.this);
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }

//        if (getActivity() != null) {
//            getActivity().runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    getLoaderManager().restartLoader(Queries.CurrentGamesQuery._TOKEN, null, CurrentGamesListFragment.this);
//                }
//            });
//        }
    }

    private void setupRecyclerView(RecyclerView recycler) {
        recycler.setLayoutManager(new LinearLayoutManager(recycler.getContext()));

        mAdapter = new CurrentGamesAdapter(getActivity(), R.layout.item_current_game,
                R.layout.item_upcoming_game, this);
        recycler.setAdapter(mAdapter);
    }

    private void setupSwipeRefresh() {
        vSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                updateLiveScores(getActivity(), mGamesIds);
            }
        });
    }

    private void updateLiveScores(final Context context, String games) {
        if (TextUtils.isEmpty(games)) {
            LOGV(TAG, "No live games found");
            toggleRefreshing(false);
            return;
        }
        final String token = getString(R.string.nhlapi_LiveApiKey);
        ApiClient.getLiveGames(ApiClient.getService(), token, games, new Callback<NhlGamesList>() {
            @Override
            public void onResponse(Call<NhlGamesList> call, Response<NhlGamesList> response) {
                mLastUpdate = System.currentTimeMillis();

                try {
                    DatabaseHelper.updateGamesScores(context, response.body().getData());

                } catch (Exception e) {
                    e.printStackTrace();
                }

                toggleRefreshingOnUiThread(false);
            }

            @Override
            public void onFailure(Call<NhlGamesList> call, Throwable t) {
                toggleRefreshingOnUiThread(false);
                t.printStackTrace();
            }
        });
    }

    private static String getGamesIds(Cursor data) {
        final List<String> games = new ArrayList<>();
        do {
            games.add(data.getString(Queries.CurrentGamesQuery.REMOTE_ID));
        } while (data.moveToNext());

        if (games.size() > 0) {
            return StringUtils.joinCsv(games);
        }

        return null;
    }


    private void updateLiveScoresIfNecessary() {
        if (Long.compare(System.currentTimeMillis() - mLastUpdate, Const.LIVE_REFRESH_DELAY) > 0
                && !TextUtils.isEmpty(mGamesIds)) {
            toggleRefreshing(true);

            updateLiveScores(getActivity(), mGamesIds);
        }
    }

    @UiThread
    private void toggleRefreshing(boolean isRefreshing) {
        if (vSwipeRefresh != null) {
            vSwipeRefresh.setRefreshing(isRefreshing);
        }
    }

    private void toggleRefreshingOnUiThread(final boolean isRefreshing) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    toggleRefreshing(isRefreshing);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

