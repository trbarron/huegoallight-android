/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.philips.lighting.hue.sdk.PHAccessPoint;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.utils.HueBridgeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class BridgesAdapter extends RecyclerView.Adapter<BridgesAdapter.BridgeViewHolder> {
    private static final String TAG = makeLogTag("BridgesAdapter");

    protected final OnBridgeSelectedListener listener;
    protected final Resources resources;
    @LayoutRes
    private final int itemLayout;
    private List<PHAccessPoint> mDataset;

    public BridgesAdapter(Context context, @LayoutRes int itemLayout, OnBridgeSelectedListener listener) {
        this.listener = listener;
        this.resources = context.getResources();
        this.itemLayout = itemLayout;
    }

    @Override
    public BridgeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new BridgeViewHolder(v, listener);
    }

    @Override
    public void onBindViewHolder(BridgeViewHolder holder, int position) {
        if (mDataset == null || mDataset.size() - 1 < position || mDataset.get(position) == null) {
            return;
        }

        final PHAccessPoint accessPoint = mDataset.get(position);
        final String bridgeName = HueBridgeUtils.getBridgeName(accessPoint.getBridgeId());

        holder.id = position;
        holder.title.setText(resources.getString(R.string.prefs_hue_bridge_name,
                bridgeName));
        holder.subTitle.setText(resources.getString(R.string.prefs_hue_bridge_ip_address,
                accessPoint.getIpAddress()));
    }

    @Override
    public int getItemCount() {
        return (mDataset == null) ? 0 : mDataset.size();
    }

    public void swapDataset(List<PHAccessPoint> data) {
        this.mDataset = data;
        notifyDataSetChanged();
    }

    public void swapDataset(PHAccessPoint singleItem) {
        final List<PHAccessPoint> list = new ArrayList<PHAccessPoint>();
        list.add(singleItem);
        swapDataset(list);
    }

    static class BridgeViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private int id;
        private TextView title;
        private TextView subTitle;
        private OnBridgeSelectedListener listener;

        public BridgeViewHolder(View itemView, OnBridgeSelectedListener listener) {
            super(itemView);

            this.listener = listener;
            this.title = (TextView) itemView.findViewById(R.id.title);
            this.subTitle = (TextView) itemView.findViewById(R.id.subtitle);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (listener != null) {
                listener.onBridgeSelected(id);
            }
        }
    }

    public interface OnBridgeSelectedListener {
        void onBridgeSelected(int id);
    }
}
