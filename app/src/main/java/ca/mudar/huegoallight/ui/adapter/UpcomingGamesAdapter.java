/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.ColorInt;
import android.support.annotation.LayoutRes;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.Queries;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.utils.HockeyUtils;
import ca.mudar.huegoallight.utils.PaletteUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class UpcomingGamesAdapter extends RecyclerView.Adapter<UpcomingGamesAdapter.GameViewHolder> {
    private static final String TAG = makeLogTag("UpcomingGamesAdapter");

    protected final Context context;
    @LayoutRes
    private final int itemLayout;
    private Cursor mCursor;


    public UpcomingGamesAdapter(Context context, @LayoutRes int itemLayout) {
        this.context = context;
        this.itemLayout = itemLayout;
    }

    @Override
    public GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false);
        return new GameViewHolder(v);
    }

    @Override
    public void onBindViewHolder(GameViewHolder holder, int position) {
        if (mCursor == null || !mCursor.moveToPosition(position)) {
            return;
        }

        holder.fillGameInfo(context, mCursor);
    }

    @Override
    public int getItemCount() {
        return (mCursor == null) ? 0 : mCursor.getCount();
    }

    public void swapDataset(Cursor cursor) {
        this.mCursor = cursor;
        notifyDataSetChanged();
    }

    protected Context getContext() {
        return context;
    }

    protected Cursor getCursor() {
        return mCursor;
    }

    protected static class GameViewHolder extends RecyclerView.ViewHolder {
        private CardView vCardView;
        private View vFooter;
        private ImageView vAwayLogo;
        private ImageView vHomeLogo;
        private TextView vGameTeams;
        private TextView vGameDate;
        private NhlTeam awayTeam;
        private NhlTeam homeTeam;

        public GameViewHolder(View view) {
            super(view);

            this.vCardView = (CardView) view;
            this.vFooter = itemView.findViewById(R.id.footer);
            this.vAwayLogo = (ImageView) itemView.findViewById(R.id.away_logo);
            this.vHomeLogo = (ImageView) itemView.findViewById(R.id.home_logo);
            this.vGameTeams = (TextView) itemView.findViewById(R.id.team_vs_team);
            this.vGameDate = (TextView) itemView.findViewById(R.id.date);
        }

        public void fillGameInfo(Context context, Cursor data) {
            final Resources res = context.getResources();
            final String awaySlug = data.getString(Queries.UpcomingGamesQuery.AWAY_TEAM_SLUG);
            final String homeSlug = data.getString(Queries.UpcomingGamesQuery.HOME_TEAM_SLUG);
            final long startsAt = data.getLong(Queries.UpcomingGamesQuery.STARTS_AT);

            awayTeam = new NhlTeam(awaySlug);
            homeTeam = new NhlTeam(homeSlug);

            // Logos
            vAwayLogo.setImageResource(awayTeam.getTeamLogo());
            vHomeLogo.setImageResource(homeTeam.getTeamLogo());

            // Names
            if (vGameTeams != null) {
                vGameTeams.setText(res.getString(R.string.game_team_vs_team,
                        res.getString(awayTeam.getTeamName()),
                        res.getString(homeTeam.getTeamName())
                ));
            }

            // Game date
            if (vGameDate != null) {
                int formatFlags = DateUtils.FORMAT_ABBREV_RELATIVE |
                        DateUtils.FORMAT_SHOW_DATE |
                        DateUtils.FORMAT_SHOW_WEEKDAY;
                if (!HockeyUtils.isStartTimeTBD(startsAt)) {
                    // TBD game time is hidden
                    formatFlags |= DateUtils.FORMAT_SHOW_TIME;
                }
                vGameDate.setText(DateUtils.formatDateTime(context,
                        startsAt,
                        formatFlags
                ));
            }

            final Bitmap homeLogo = BitmapFactory.decodeResource(context.getResources(), homeTeam.getTeamLogo());
            Palette.from(homeLogo).generate(new Palette.PaletteAsyncListener() {
                public void onGenerated(Palette p) {
                    try {
                        if (vCardView != null) {
                            final Palette.Swatch swatch = PaletteUtils.getDominantSwatch(p);
                            if (swatch == null) {
                                return;
                            }
                            vFooter.setBackgroundColor(swatch.getRgb());
                            setTextColor(swatch.getBodyTextColor());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        protected NhlTeam getAwayTeam() {
            return awayTeam;
        }

        protected NhlTeam getHomeTeam() {
            return homeTeam;
        }

        protected View getFooterView() {
            return vFooter;
        }

        protected void setTextColor(@ColorInt int color) {
            vGameTeams.setTextColor(color);
            vGameDate.setTextColor(color);
        }
    }
}
