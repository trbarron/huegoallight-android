/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ProgressBar;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class PushlinkFragment extends Fragment {
    private static final String TAG = makeLogTag("PushlinkFragment");

    private static final int PROGRESS_BAR_MAX = 3000;
    private static final long PROGRESS_BAR_DURATION = 30000L; // 30 seconds, authentication timeout
    private static final long PULSE_DURATION = 2000L; // 2 seconds pulse
    private static final int WRONG_CLICK_THRESHOLD = 3;

    private ProgressBar vProgressBar;

    public static PushlinkFragment newInstance() {
        final PushlinkFragment fragment = new PushlinkFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_pushlink, container, false);

        final ImageView illustration = (ImageView) view.findViewById(R.id.illustration);
        vProgressBar = (ProgressBar) view.findViewById(R.id.progress);

        animateBridgeColor(illustration);
        setupOnWrongClickListener(illustration);
        startCountdown(savedInstanceState);

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.BundleKeys.PROGRESS_VALUE, vProgressBar.getProgress());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mAnimator.cancel();
    }

    private void startCountdown(Bundle savedInstanceState) {
        int startValue = PROGRESS_BAR_MAX;
        long duration = PROGRESS_BAR_DURATION;

        if (savedInstanceState != null) {
            // Restore countdown value
            startValue = savedInstanceState.getInt(Const.BundleKeys.PROGRESS_VALUE, PROGRESS_BAR_MAX);
            duration = (PROGRESS_BAR_DURATION * startValue) / PROGRESS_BAR_MAX;
        }

        final ObjectAnimator animation = ObjectAnimator.ofInt(vProgressBar, "progress", startValue, 0);
        animation.setDuration(duration);
        animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
    }

    ValueAnimator mAnimator;

    private void animateBridgeColor(final ImageView imageView) {
        mAnimator = new ValueAnimator();
        mAnimator.setIntValues(0, 192);
        mAnimator.setDuration(PULSE_DURATION);
        mAnimator.setRepeatCount((int) (PROGRESS_BAR_DURATION / PULSE_DURATION) - 1);
        mAnimator.setRepeatMode(ValueAnimator.REVERSE);
        mAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                final int alpha = (Integer) animation.getAnimatedValue();
                imageView.setColorFilter(Color.argb(alpha, 0, 0, 0));
            }
        });
        mAnimator.start();
    }

    private int totalClicks = 0;

    /**
     * In case user clicks on the illustration instead of the physical Push-link button!
     * @param view
     */
    private void setupOnWrongClickListener(View view) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                totalClicks++;
                if (totalClicks >= WRONG_CLICK_THRESHOLD) {
                    totalClicks = 0;
                    Snackbar.make(getView(), R.string.snackbar_pushlink_wrong_click, Snackbar.LENGTH_LONG)
                            .setAction(R.string.btn_help, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(Const.HUE_WEBSITE)));
                                }
                            })
                            .show();
                }
            }
        });
    }
}
