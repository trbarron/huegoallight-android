/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.philips.lighting.model.PHLight;
import com.woxthebox.draglistview.DragItemAdapter;

import ca.mudar.huegoallight.R;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class LightsDragItemAdapter extends DragItemAdapter<PHLight, LightsDragItemAdapter.LightViewHolder> {
    private static final String TAG = makeLogTag("LightsDragItemAdapter");

    public LightsDragItemAdapter(Context context) {
        setHasStableIds(true);
    }

    @Override
    public long getItemId(int position) {
        return mItemList.get(position).getUniqueId().hashCode();
    }

    @Override
    public LightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lights_grid, parent, false);
        return new LightViewHolder(v, R.id.cardview);
    }

    @Override
    public void onBindViewHolder(LightViewHolder holder, int position) {
        if (mItemList == null || mItemList.size() - 1 < position || mItemList.get(position) == null) {
            return;
        }
        super.onBindViewHolder(holder, position);

        final PHLight light = mItemList.get(position);

        holder.title.setText(light.getName());
        holder.status.setVisibility(light.getLastKnownLightState().isReachable() ?
                View.GONE : View.VISIBLE);
    }

    static class LightViewHolder extends DragItemAdapter.ViewHolder {
        private TextView title;
        private TextView status;

        public LightViewHolder(View itemView, int handleResId) {
            super(itemView, handleResId, false);

            this.title = (TextView) itemView.findViewById(R.id.title);
            this.status = (TextView) itemView.findViewById(R.id.status);
        }
    }
}
