/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.activity;

import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.squareup.otto.Subscribe;

import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.service.TopicsUpdateService;
import ca.mudar.huegoallight.ui.adapter.NhlTeamsAdapter;
import ca.mudar.huegoallight.ui.fragment.TeamsListFragment;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class OnboardingActivity extends AppCompatActivity implements
        SyncBusListener,
        NhlTeamsAdapter.OnTeamSelectedListener {
    private static final String TAG = makeLogTag("OnboardingActivity");

    private PHHueSDK hueSDK;
    private HueSDKListener mHueSDKListener;

    private int mAccessPointsNumber = Const.UNKNOWN_VALUE; // Value to send to MainActivity
    private String mSelectedTeam;

    public static Intent newIntent(Context context) {
        return new Intent(context, OnboardingActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            final Fragment fragment = TeamsListFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(android.R.id.content, fragment, Const.FragmentTags.TEAMS)
                    .commit();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        registerSyncBus();

        setupHueBridge();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(Const.BundleKeys.NB_ACCESS_POINTS, mAccessPointsNumber);
        outState.putString(Const.BundleKeys.TEAM_SLUG, mSelectedTeam);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        mAccessPointsNumber = savedInstanceState.getInt(Const.BundleKeys.NB_ACCESS_POINTS, Const.UNKNOWN_VALUE);
        mSelectedTeam = savedInstanceState.getString(Const.BundleKeys.TEAM_SLUG, null);
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterSyncBus();
    }

    /**
     * Implements NhlTeamsAdapter.OnTeamSelectedListener
     * Starts the Hue Push-link activity (Onboarding #2)
     *
     * @param team
     */
    @Override
    public void onTeamSelected(String team) {
        mSelectedTeam = team;
        finishWhenDone();
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Subscribe
    public void onAccessPointFound(SyncBusEvents.AccessPointFound event) {
        mAccessPointsNumber = 1;
        finishWhenDone();
    }

    @Subscribe
    public void onMultiAccessPointsFound(SyncBusEvents.MultiAccessPointsFound event) {
        final List<PHAccessPoint> accessPoints = hueSDK.getAccessPointsFound();
        mAccessPointsNumber = accessPoints == null ? 0 : accessPoints.size();
        finishWhenDone();
    }

    @Subscribe
    public void onBridgeAuthRequired(SyncBusEvents.BridgeAuthRequired event) {
        // Authentication will be done later
        hueSDK.stopPushlinkAuthentication();
    }

    @Subscribe
    public void onBridgeError(SyncBusEvents.BridgeError error) {
        mAccessPointsNumber = 0;
        finishWhenDone();
    }

    private void setupHueBridge() {
        hueSDK = PHHueSDK.getInstance();
        mHueSDKListener = HueBridgeUtils.initializeHueBridge(new ContextWrapper(this), hueSDK, true);
    }

    /**
     * First Onboarding is closed after user has selected a team,
     * and Hue SDK found a Bridge (or BridgeNotFound error)
     */
    private void finishWhenDone() {
        if (!TextUtils.isEmpty(mSelectedTeam) && mAccessPointsNumber >= 0) {
            // Set as default team and clear Onboarding flag
            final HuePrefs prefs = HuePrefs.getInstance(getApplicationContext());
            prefs.setDefaultTeam(mSelectedTeam);
            prefs.setOnboardingCompleted();

            // Subscribe to Team topic
            startService(TopicsUpdateService.newIntent(this, mSelectedTeam, true));

            // Next Onboarding screen: HueSetup
            startActivity(MainActivity.newIntent(this, true, mAccessPointsNumber));
            finish();
        }
    }
}
