/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.UiThread;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;

import com.squareup.otto.Subscribe;

import java.util.Locale;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.model.NhlTeam;
import ca.mudar.huegoallight.service.TopicsUpdateService;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;
import ca.mudar.huegoallight.ui.listener.SyncBusListener;
import ca.mudar.huegoallight.utils.ConnectionUtils;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class SettingsNhlTeamsFragment extends PreferenceFragment implements
        Const.PrefsNames,
        SyncBusListener,
        SharedPreferences.OnSharedPreferenceChangeListener {
    private static final String TAG = makeLogTag("SettingsNhlTeamsFragment");

    public static SettingsNhlTeamsFragment newInstance() {
        return new SettingsNhlTeamsFragment();
    }

    private SharedPreferences mSharedPrefs;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final PreferenceManager pm = this.getPreferenceManager();
        pm.setSharedPreferencesName(Const.APP_PREFS_NAME);
        pm.setSharedPreferencesMode(Context.MODE_PRIVATE);

        addPreferencesFromResource(R.xml.prefs_nhl_teams);

        mSharedPrefs = pm.getSharedPreferences();
    }

    @Override
    public void onResume() {
        super.onResume();

        mSharedPrefs.registerOnSharedPreferenceChangeListener(this);

        registerSyncBus();
    }

    @Override
    public void onPause() {
        super.onPause();

        mSharedPrefs.unregisterOnSharedPreferenceChangeListener(this);

        unregisterSyncBus();
    }

    /**
     * Implements OnSharedPreferenceChangeListener
     *
     * @param sharedPreferences
     * @param key
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (!ConnectionUtils.checkConnection(getActivity(), getView())) {
            undoPreferenceChange((CheckBoxPreference) findPreference(key));
            return;
        }
        if (TextUtils.isEmpty(key) || !key.startsWith(NHL_TEAM_PREFIX)) {
            return;
        }

        final String team = key.replace(NHL_TEAM_PREFIX, "")
                .toUpperCase(Locale.getDefault());
        if (((CheckBoxPreference) findPreference(key)).isChecked()) {
            HuePrefs.getInstance(getActivity().getApplicationContext()).followNhlTeam(team);
            getActivity().startService(
                    TopicsUpdateService.newIntent(getActivity(), team, true));
        } else {
            HuePrefs.getInstance(getActivity().getApplicationContext()).unfollowNhlTeam(team);
            getActivity().startService(
                    TopicsUpdateService.newIntent(getActivity(), team, false));
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void registerSyncBus() {
        try {
            HueGoalApp.getSyncBus().register(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    /**
     * Implements SyncBusListener
     */
    @Override
    public void unregisterSyncBus() {
        try {
            HueGoalApp.getSyncBus().unregister(this);
        } catch (IllegalArgumentException e) {
            LogUtils.REMOTE_LOG(e);
        }
    }

    @Subscribe
    public void onTeamFollowError(SyncBusEvents.GcmTopicsError error) {
        // Selected Preference key
        final String key = NHL_TEAM_PREFIX +
                error.getTeam().toLowerCase(Locale.getDefault());
        // Team name
        final String teamName = getResources().getString(new NhlTeam(error.getTeam()).getTeamShortName());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    final CheckBoxPreference checkBox = (CheckBoxPreference) findPreference(key);
                    final String message = getResources().getString(
                            checkBox.isChecked() ? R.string.snackbar_gcm_topics_subscribe_error : R.string.snackbar_gcm_topics_unsubscribe_error,
                            teamName);

                    undoPreferenceChange(checkBox);
                    checkBox.setEnabled(false);

                    Snackbar.make(SettingsNhlTeamsFragment.this.getView(),
                            message,
                            Snackbar.LENGTH_LONG
                    ).show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Toggle prefs listener before and after reverting the value
     *
     * @param checkBox
     */
    @UiThread
    private void undoPreferenceChange(CheckBoxPreference checkBox) {
        mSharedPrefs.unregisterOnSharedPreferenceChangeListener(SettingsNhlTeamsFragment.this);
        checkBox.setChecked(!checkBox.isChecked());
        mSharedPrefs.registerOnSharedPreferenceChangeListener(SettingsNhlTeamsFragment.this);
    }
}
