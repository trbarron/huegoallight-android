/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHLight;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.LightsColumns;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.dialog.LightsBoardHelpFragment;
import ca.mudar.huegoallight.ui.widget.LightsBoardView;
import ca.mudar.huegoallight.utils.HueBlinkHelper;
import ca.mudar.huegoallight.utils.LogUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class LightsBoardFragment extends Fragment implements
        LightsBoardView.BoardListener {
    private static final String TAG = makeLogTag("LightsBoardFragment");

    private LightsBoardView mLightsBoard;
    private HuePrefs mPrefs;

    public static LightsBoardFragment newInstance() {
        return new LightsBoardFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        mPrefs = HuePrefs.getInstance(getContext().getApplicationContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_lights_board, container, false);

        mLightsBoard = (LightsBoardView) view.findViewById(R.id.board_view);
        mLightsBoard.setBoardListener(this);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        addBoardLightColumns(mLightsBoard);
    }

    @Override
    public void onResume() {
        super.onResume();

        mLightsBoard.post(new Runnable() {
            @Override
            public void run() {
                try {
                    mLightsBoard.scrollToInitialColumn();
                    showDiscoveryIfNecessary();
                } catch (Exception e) {
                    LogUtils.REMOTE_LOG(e);
                }
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_help, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_help) {
            final LightsBoardHelpFragment bottomSheet = LightsBoardHelpFragment.newInstance();
            bottomSheet.show(getFragmentManager(), Const.FragmentTags.LIGHTS_BOARD_HELP);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();

        mPrefs.saveLastBridgeSelectedLights();
    }

    /**
     * Implements BoardView.BoardListener
     *
     * @param column
     * @param row
     */
    @Override
    public void onItemDragStarted(int column, int row) {
        // TODO set initial light bulb color
    }

    /**
     * Implements BoardView.BoardListener
     *
     * @param oldColumn
     * @param newColumn
     */
    @Override
    public void onItemChangedColumn(int oldColumn, int newColumn) {
        // TODO set new light bulb color
    }

    /**
     * Implements BoardView.BoardListener
     *
     * @param fromColumn
     * @param fromRow
     * @param toColumn
     * @param toRow
     */
    @Override
    public void onItemDragEnded(int fromColumn, int fromRow, int toColumn, int toRow) {
        if (fromColumn == toColumn) {
            // nothing to do here
            return;
        }
        // TODO blink lightbulb once

        final List<PHLight> primaryLights = mLightsBoard.getColumnLights(LightsColumns.PRIMARY_COLOR);
        final List<PHLight> secondaryLights = mLightsBoard.getColumnLights(LightsColumns.SECONDARY_COLOR);
        final List<PHLight> redLights = mLightsBoard.getColumnLights(LightsColumns.RED_LIGHT);

        mPrefs.setSelectedLights(getLightsIds(primaryLights), getLightsIds(secondaryLights),
                getLightsIds(redLights));
    }

    private void showDiscoveryIfNecessary() {
        if (mPrefs.isLightsBoardDiscovery()) {
            mLightsBoard.showDiscovery(getActivity());
            mPrefs.setLightsBoardDiscoveryCompleted();
        }
    }

    private void addBoardLightColumns(final LightsBoardView lightsBoard) {
        List<PHLight> allLights;
        try {
            allLights = PHHueSDK.getInstance().getSelectedBridge().getResourceCache().getAllLights();
        } catch (NullPointerException e) {
            allLights = new ArrayList<>();
        }

        final List<String> selectedPrimary = mPrefs.getPrimaryLights();
        final List<String> selectedSecondary = mPrefs.getSecondaryLights();
        final List<String> selectedRed = mPrefs.getRedLights();

        final List<PHLight> primaryLights = new ArrayList<>();
        final List<PHLight> secondaryLights = new ArrayList<>();
        final List<PHLight> redLights = new ArrayList<>();
        final List<PHLight> unusedLights = new ArrayList<>();

        for (PHLight light : allLights) {
            if (selectedPrimary.contains(light.getUniqueId())) {
                primaryLights.add(light);
            } else if (selectedSecondary.contains(light.getUniqueId())) {
                secondaryLights.add(light);
            } else if (selectedRed.contains(light.getUniqueId())) {
                redLights.add(light);
            } else {
                unusedLights.add(light);
            }

            // Set AlertMode to NONE, just in case.
            HueBlinkHelper.resetLightBlinkState(PHHueSDK.getInstance(), light);
        }

        lightsBoard.addLightsColumnList(LightsColumns.UNUSED_LIGHTS, unusedLights);
        lightsBoard.addLightsColumnList(LightsColumns.PRIMARY_COLOR, primaryLights);
        lightsBoard.addLightsColumnList(LightsColumns.SECONDARY_COLOR, secondaryLights);
        lightsBoard.addLightsColumnList(LightsColumns.RED_LIGHT, redLights);
    }

    @NonNull
    private List<String> getLightsIds(List<PHLight> lights) {
        final List<String> lightsIds = new ArrayList<>();
        if (lights != null) {
            for (PHLight light : lights) {
                lightsIds.add(light.getUniqueId());
            }
        }

        return lightsIds;
    }
}
