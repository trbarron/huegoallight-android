/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.ui.view;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.woxthebox.draglistview.DragItem;

import ca.mudar.huegoallight.R;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class LightDragView extends DragItem {
    private static final String TAG = makeLogTag("LightDragView");

    private final static String CARD_ELEVATION = "CardElevation";
    private final float elevation;
    private final float maxElevation;

    public LightDragView(Context context, int layoutId) {
        super(context, layoutId);

        final Resources res = context.getResources();
        this.elevation = res.getDimension(R.dimen.board_card_elevation);
        this.maxElevation = res.getDimension(R.dimen.board_card_max_elevation);
    }

    @Override
    public void onBindDragView(View clickedView, View dragView) {
        final CharSequence title = ((TextView) clickedView.findViewById(R.id.title)).getText();
        final int statusVisibility = clickedView.findViewById(R.id.status).getVisibility();

        ((TextView) dragView.findViewById(R.id.title)).setText(title);
        dragView.findViewById(R.id.status).setVisibility(statusVisibility);

        final CardView dragCard = ((CardView) dragView.findViewById(R.id.cardview));

        dragCard.setCardElevation(elevation);
        dragCard.setMaxCardElevation(maxElevation);
    }

    @Override
    public void onMeasureDragView(View clickedView, View dragView) {
        final CardView dragCard = ((CardView) dragView.findViewById(R.id.cardview));
        final CardView clickedCard = ((CardView) clickedView.findViewById(R.id.cardview));

        final int widthDiff = dragCard.getPaddingLeft() - clickedCard.getPaddingLeft() + dragCard.getPaddingRight() -
                clickedCard.getPaddingRight();
        final int heightDiff = dragCard.getPaddingTop() - clickedCard.getPaddingTop() + dragCard.getPaddingBottom() -
                clickedCard.getPaddingBottom();
        final int width = clickedView.getMeasuredWidth() + widthDiff;
        final int height = clickedView.getMeasuredHeight() + heightDiff;
        dragView.setLayoutParams(new FrameLayout.LayoutParams(width, height));

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(width, View.MeasureSpec.EXACTLY);
        final int heightSpec = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY);
        dragView.measure(widthSpec, heightSpec);
    }

    @Override
    public void onStartDragAnimation(View dragView) {
        final CardView dragCard = ((CardView) dragView.findViewById(R.id.cardview));
        final ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, CARD_ELEVATION, dragCard.getCardElevation(), maxElevation);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(ANIMATION_DURATION);
        anim.start();
    }

    @Override
    public void onEndDragAnimation(View dragView) {
        final CardView dragCard = ((CardView) dragView.findViewById(R.id.cardview));
        final ObjectAnimator anim = ObjectAnimator.ofFloat(dragCard, CARD_ELEVATION, dragCard.getCardElevation(), elevation);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setDuration(ANIMATION_DURATION);
        anim.start();
    }
}
