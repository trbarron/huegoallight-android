/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.v4.graphics.drawable.DrawableCompat;

public class ImageUtils {

    public static Drawable getScaledDrawable(Context context, int iconResId, int newWidth, int newHeight) {
        // Load the Bitmap
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inScaled = false;
        options.inPreferQualityOverSpeed = true;
        final Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(),
                iconResId, options);

        // Calculate the scale
        final int width = bitmap.getWidth();
        final int height = bitmap.getHeight();
        final float scaleWidth = (float) newWidth / width;
        final float scaleHeight = (float) newHeight / height;

        // Create a resize matrix
        final Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        // Get the new Drawable
        final BitmapDrawable bitmapDrawable = new BitmapDrawable(context.getResources(),
                Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true));
        // Recycle the Bitmap
        if (!bitmap.isRecycled()) {
            bitmap.recycle();
        }

        // Return scaled Drawable
        return bitmapDrawable;
    }

    public static Drawable tintDrawable(Drawable drawable, @ColorInt int color) {
        final Drawable wrappedDrawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(wrappedDrawable, color);

        return wrappedDrawable;
    }
}
