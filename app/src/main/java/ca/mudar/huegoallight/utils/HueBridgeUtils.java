/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.content.ContextWrapper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.philips.lighting.hue.sdk.PHAccessPoint;
import com.philips.lighting.hue.sdk.PHBridgeSearchManager;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.util.List;
import java.util.Locale;

import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.listener.HueSDKListener;

import static ca.mudar.huegoallight.Const.PrefsValues.BRIDGE_ANONYMOUS;
import static ca.mudar.huegoallight.utils.LogUtils.LOGV;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class HueBridgeUtils {
    private static final String TAG = makeLogTag("HueBridgeUtils");

    public static HueSDKListener initializeHueBridge(ContextWrapper contextWrapper, PHHueSDK hueSDK) {
        return initializeHueBridge(contextWrapper, hueSDK, false);
    }

    public static HueSDKListener initializeHueBridge(ContextWrapper contextWrapper, PHHueSDK hueSDK, boolean forceSearch) {
        final HueSDKListener listener = ((HueGoalApp) contextWrapper.getApplicationContext()).getHueListener();
        try {
            final PHAccessPoint accessPoint = HueBridgeUtils.getLastAccessPoint(contextWrapper.getApplicationContext());
            if (accessPoint == null || forceSearch) {
                LOGV(TAG, "UPNP search of local bridges");
                // Start the UPNP search of local bridges.
                PHBridgeSearchManager sm = (PHBridgeSearchManager) hueSDK.getSDKService(PHHueSDK.SEARCH_BRIDGE);
                sm.search(true, true);
            } else if (!hueSDK.isAccessPointConnected(accessPoint)) {
                hueSDK.connect(accessPoint);
            }
        } catch (Exception e) {
            LogUtils.REMOTE_LOG(e);
        }

        return listener;
    }

    public static boolean isConnected(Context context, PHHueSDK hueSDK) {
        final HuePrefs prefs = HuePrefs.getInstance(context);

        final String ipAddress = prefs.getIPAddress();
        final String username = prefs.getUsername();

        // Automatically try to connect to the last connected IP Address.
        if (ipAddress != null && !ipAddress.isEmpty()) {
            final PHAccessPoint lastAccessPoint = new PHAccessPoint();
            lastAccessPoint.setIpAddress(ipAddress);
            lastAccessPoint.setUsername(username);

            return hueSDK.isAccessPointConnected(lastAccessPoint);
        }

        return false;
    }

    public static boolean connectToBridgeIfNecessary(Context context, PHHueSDK hueSDK) {

        return false;
    }

    public static void disconnectBridgeIfNecessary(PHHueSDK hueSDK) {
        if (hueSDK != null) {
            try {
                final List<PHBridge> allBridges = hueSDK.getAllBridges();
                if (allBridges == null || allBridges.isEmpty()) {
                    return;
                }

                hueSDK.disableAllHeartbeat();

                final PHBridge bridge = hueSDK.getSelectedBridge();
                if (bridge != null) {
                    hueSDK.disconnect(bridge);
                }
            } catch (Exception e) {
                LogUtils.REMOTE_LOG(e);
            }
        }
    }

    public static PHAccessPoint getLastAccessPoint(Context context) {
        final HuePrefs prefs = HuePrefs.getInstance(context);

        final String ipAddress = prefs.getIPAddress();
        final String username = prefs.getUsername();

        // Automatically try to connect to the last connected IP Address.
        if (ipAddress != null && !ipAddress.isEmpty()) {
            final PHAccessPoint lastAccessPoint = new PHAccessPoint();
            lastAccessPoint.setIpAddress(ipAddress);
            lastAccessPoint.setUsername(username);

            return lastAccessPoint;
        }

        return null;
    }

    public static boolean isLastAccessPoint(@NonNull Context context, PHAccessPoint accessPoint) {
        if (accessPoint == null) {
            return false;
        }
        final HuePrefs prefs = HuePrefs.getInstance(context);

        final String ipAddress = prefs.getIPAddress();
        final String bridgeId = prefs.getBridgeId();
        if (TextUtils.isEmpty(ipAddress) || TextUtils.isEmpty(bridgeId)) {
            return false;
        }

        return ipAddress.equals(accessPoint.getIpAddress()) &&
                bridgeId.equals(accessPoint.getBridgeId());
    }

    public static String getBridgeName(String macAddress) {
        if (!TextUtils.isEmpty(macAddress)) {
            if (macAddress.length() == 17) {
                return macAddress.replace(":", "").substring(6);
            }
            if (macAddress.length() == 16) {
                return macAddress.substring(10);
            }
        }

        return macAddress;
    }

    public static String getNormalizedBridgeId(String macAddress) {
        if (!TextUtils.isEmpty(macAddress)) {
            String normalized = Normalizer.normalize(macAddress.toLowerCase(Locale.ROOT),
                    Normalizer.Form.NFD);
            String result = normalized.replaceAll("[^A-Za-z0-9]", "");
            if (!TextUtils.isEmpty(result)) {
                return result;
            }
        }

        return BRIDGE_ANONYMOUS;
    }

    public static String getRandomUsername() {
        return new BigInteger(130, new SecureRandom()).toString(32);
    }

    /**
     * Clone LightState values: on/off, brightness, colorMode and its related color values.
     * Doesn't clone alert/effect modes
     *
     * @param state The state to clone
     * @return new similar state
     */
    @Nullable
    public static PHLightState cloneLightState(final PHLightState state) {
        if (state == null) {
            return null;
        }

        final PHLightState cloneState = new PHLightState();

        cloneState.setOn(state.isOn());
        cloneState.setBrightness(state.getBrightness());

        final PHLight.PHLightColorMode colorMode = state.getColorMode();
        cloneState.setColorMode(colorMode);
        switch (colorMode) {
            case COLORMODE_XY:
                cloneState.setX(state.getX());
                cloneState.setY(state.getY());
                break;
            case COLORMODE_CT:
                cloneState.setCt(state.getCt());
                break;
            case COLORMODE_HUE_SATURATION:
                cloneState.setHue(state.getHue());
                cloneState.setSaturation(state.getSaturation());
                break;
        }

        return cloneState;
    }

    /**
     * Safe method to compare two AlertModes
     *
     * @param a
     * @param b
     * @return true if string values of both modes are equal
     */
    public static boolean areEqualModes(PHLight.PHLightAlertMode a, PHLight.PHLightAlertMode b) {
        final String v1 = (a != null) ? a.getValue() : null;
        final String v2 = (b != null) ? b.getValue() : null;

        return TextUtils.equals(v1, v2);
    }
}
