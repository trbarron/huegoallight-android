/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class TimeUtils {

    public static int getMinutesOfDay(Calendar calendar) {
        return getMinutesOfDay(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE));
    }

    public static int getMinutesOfDay(int hourOfDay, int minute) {
        return (hourOfDay * 60) + minute;
    }

    public static Calendar getCalendarInstance(int minutesOfDay) {
        final Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, minutesOfDay / 60);
        calendar.set(Calendar.MINUTE, minutesOfDay % 60);

        return calendar;
    }

    public static String getTimeDisplay(Context context, int minutesOfDay) {
        return getTimeDisplay(context, getCalendarInstance(minutesOfDay));
    }

    public static String getTimeDisplay(Context context, Calendar calendar) {
        SimpleDateFormat format;
        if (DateFormat.is24HourFormat(context)) {
            format = new SimpleDateFormat("H:mm", Locale.getDefault());
        } else {
            format = new SimpleDateFormat("K:mm aa", Locale.getDefault());
        }
        return format.format(calendar.getTime());
    }

    public static boolean isCurrentlyWithinPeriod(Calendar start, Calendar end) {
        final Calendar now = Calendar.getInstance();

        if (now.equals(start) || now.equals(end) || start.equals(end)) {
            return true;
        } else if (start.before(end)) {
            return start.before(now) && end.after(now);
        } else if (end.before(start)) {
            // Overnight period, inverse the condition
            return !(end.before(now) && start.after(now));
        }

        return false;
    }
}
