/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;

import ca.mudar.huegoallight.R;

public class ConnectionUtils {
    public static boolean hasConnection(final Context context) {
        final ConnectivityManager conMan = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = conMan.getActiveNetworkInfo();
        if (networkInfo == null) {
            return false;
        } else {
            return networkInfo.isConnected();
        }
    }

    public static boolean checkConnection(final Context context, final View view) {
        if (!hasConnection(context)) {
            Snackbar.make(view,
                    R.string.snackbar_no_connection,
                    Snackbar.LENGTH_INDEFINITE
            ).setAction(R.string.snackbar_btn_retry, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkConnection(context, view);
                }
            }).show();
            return false;
        }

        return true;
    }
}
