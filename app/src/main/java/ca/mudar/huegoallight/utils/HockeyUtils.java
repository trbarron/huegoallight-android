/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.model.NhlTeam;

public class HockeyUtils {

    public static int getTeamNotifyIcon(String slug) {
//        switch (slug) {
//            case "ANA":
//                return R.drawable.ic_notify_ana;
//            case "ARI":
//                return R.drawable.ic_notify_ari;
//            case "BOS":
//                return R.drawable.ic_notify_bos;
//            case "BUF":
//                return R.drawable.ic_notify_buf;
//            case "CAR":
//                return R.drawable.ic_notify_car;
//            case "CBJ":
//                return R.drawable.ic_notify_cbj;
//            case "CGY":
//                return R.drawable.ic_notify_cgy;
//            case "CHI":
//                return R.drawable.ic_notify_chi;
//            case "COL":
//                return R.drawable.ic_notify_col;
//            case "DAL":
//                return R.drawable.ic_notify_dal;
//            case "DET":
//                return R.drawable.ic_notify_det;
//            case "EDM":
//                return R.drawable.ic_notify_edm;
//            case "FLA":
//                return R.drawable.ic_notify_fla;
//            case "LAK":
//                return R.drawable.ic_notify_lak;
//            case "MIN":
//                return R.drawable.ic_notify_min;
//            case "MTL":
//                return R.drawable.ic_notify_mtl;
//            case "NJD":
//                return R.drawable.ic_notify_njd;
//            case "NSH":
//                return R.drawable.ic_notify_nsh;
//            case "NYI":
//                return R.drawable.ic_notify_nyi;
//            case "NYR":
//                return R.drawable.ic_notify_nyr;
//            case "OTT":
//                return R.drawable.ic_notify_ott;
//            case "PHI":
//                return R.drawable.ic_notify_phi;
//            case "PIT":
//                return R.drawable.ic_notify_pit;
//            case "SJS":
//                return R.drawable.ic_notify_sjs;
//            case "STL":
//                return R.drawable.ic_notify_stl;
//            case "TBL":
//                return R.drawable.ic_notify_tbl;
//            case "TOR":
//                return R.drawable.ic_notify_tor;
//            case "VAN":
//                return R.drawable.ic_notify_van;
//            case "VGK":
//                return R.drawable.ic_notify_vgk;
//            case "WPG":
//                return R.drawable.ic_notify_wpg;
//            case "WSH":
//                return R.drawable.ic_notify_wsh;
//        }

        return R.drawable.ic_notify_empty;
    }

    /**
     * Returns NHL Eastern conference teams, sorted by city
     *
     * @return
     */
    public static String[] getEasternConferenceTeams() {
        return new String[]{
                "BOS", "BUF", "CAR", "CBJ", "DET",
                "FLA", "MTL", "NJD", "NYI", "NYR",
                "OTT", "PHI", "PIT", "TBL", "TOR",
                "WSH"
        };
    }

    /**
     * Returns NHL Western conference teams, sorted by city
     *
     * @return
     */
    public static String[] getWesternConferenceTeams() {
        return new String[]{
                "ANA", "ARI", "CGY", "CHI", "COL",
                "DAL", "EDM", "LAK", "MIN", "NSH",
                "SJS", "STL", "VAN", "VGK", "WPG"
        };
    }

    /**
     * Returns NHL teams, sorted by short name
     *
     * @return
     */
    public static List<NhlTeam> getNhlTeams() {
        final String[] slugs = new String[]{
                "COL", "CHI", "CBJ", "STL", "BOS",
                "MTL", "VAN", "WSH", "ARI", "NJD",
                "ANA", "CGY", "PHI", "VGK", "CAR",
                "NYI", "WPG", "LAK", "TBL", "TOR",
                "EDM", "FLA", "PIT", "NSH", "NYR",
                "DET", "BUF", "OTT", "SJS", "DAL",
                "MIN"
        };

        final List<NhlTeam> teams = new ArrayList<>();
        for (String slug : slugs) {
            teams.add(new NhlTeam(slug));
        }

        return teams;
    }

    /**
     * Check if start time is known. The API sends Midnight (Eastern) for TBD game time
     *
     * @param timestamp
     * @return true if startTime is to-be-determined
     */
    public static boolean isStartTimeTBD(long timestamp) {
        final GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTimeZone(TimeZone.getTimeZone(Const.EAST_TIMEZONE));
        calendar.setTimeInMillis(timestamp);

        return (calendar.get(Calendar.HOUR_OF_DAY) == 0 && calendar.get(Calendar.MINUTE) == 0);
    }
}
