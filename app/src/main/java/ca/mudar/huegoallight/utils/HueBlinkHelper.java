/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.text.format.DateUtils;

import com.philips.lighting.hue.listener.PHLightListener;
import com.philips.lighting.hue.sdk.PHHueSDK;
import com.philips.lighting.hue.sdk.exception.PHHueException;
import com.philips.lighting.hue.sdk.utilities.PHUtilities;
import com.philips.lighting.model.PHBridge;
import com.philips.lighting.model.PHBridgeResource;
import com.philips.lighting.model.PHHueError;
import com.philips.lighting.model.PHLight;
import com.philips.lighting.model.PHLightState;
import com.squareup.otto.Bus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.HueGoalApp;
import ca.mudar.huegoallight.data.HuePrefs;
import ca.mudar.huegoallight.ui.listener.SyncBusEvents;

import static ca.mudar.huegoallight.Const.ALTERNATE_RGB_COLOR;
import static ca.mudar.huegoallight.Const.HUE_DEFAULT_TRANSITION;
import static ca.mudar.huegoallight.Const.HUE_FAST_TRANSITION;
import static ca.mudar.huegoallight.utils.LogUtils.LOGW;

public class HueBlinkHelper {
    private static final String TAG = "HueBlinkHelper";

    // Better to wait 100ms after the default transition to switch lights off
    private static final int DELAY_SWITCH_OFF = HUE_DEFAULT_TRANSITION + 100;

    private final PHHueSDK hueSDK;
    private final List<Integer> mBlinkHues;
    private final int blinkDuration;
    private final boolean isOpponent;
    @Deprecated
    private boolean isManualBlink;
    private Map<String, PHLightState> previousStates;
    // TODO refactor this, using a hashmap or a PHLightState wrapper
    private List<String> primaryLights;
    private List<String> secondaryLights;
    private List<String> redLights;
    private Bus bus;

    public HueBlinkHelper(Context context, List<Integer> hues) {
        this(context, hues, null);

        this.isManualBlink = true;
    }

    public HueBlinkHelper(Context context, List<Integer> hues, String team) {
        this.hueSDK = PHHueSDK.getInstance();
        this.bus = HueGoalApp.getSyncBus();
        this.mBlinkHues = hues;

        final HuePrefs prefs = HuePrefs.getInstance(context);
        this.blinkDuration = prefs.getBlinkDuration();
        this.primaryLights = prefs.getPrimaryLights();
        this.secondaryLights = prefs.getSecondaryLights();
        this.redLights = prefs.getRedLights();
        this.isOpponent = !prefs.isFollowingTeam(team);
        this.previousStates = new HashMap<>();
    }

    /**
     * Set light alertMode to none. Strangely, AlertMode remains ALERT_SELECT/LSELECT till
     * manually reset, even when idle. This means the app could mistakenly ignore some lights.
     *
     * @param hueSDK
     * @param light
     */
    public static void resetLightBlinkState(PHHueSDK hueSDK, PHLight light) {
        if (hueSDK != null && light != null) {
            try {
                final PHBridge bridge = hueSDK.getSelectedBridge();
                if (bridge != null) {
                    final PHLightState noBlinkState = new PHLightState();
                    noBlinkState.setAlertMode(PHLight.PHLightAlertMode.ALERT_NONE);

                    bridge.updateLightState(light, noBlinkState);
                }
            } catch (PHHueException e) {
                e.printStackTrace();
            }
        }
    }

    public void startBlinking() {
        final PHBridge bridge = hueSDK.getSelectedBridge();
        if (bridge == null) {
            return;
        } else if (primaryLights.size() == 0 && secondaryLights.size() == 0 && redLights.size() == 0) {
            bus.post(new SyncBusEvents.LightsSelectionError());
            return;
        }

        // Clear any previous values
        this.previousStates = new HashMap<>();

        final PHLightState blinkState = new PHLightState();
        blinkState.setAlertMode(PHLight.PHLightAlertMode.ALERT_LSELECT);

        bus.post(new SyncBusEvents.LightBlinkStarted());

        for (final PHLight light : bridge.getResourceCache().getAllLights()) {
            final Integer blinkHue;
            if (isPrimaryLight(light)) {
                blinkHue = getPrimaryColor(mBlinkHues);
            } else if (isSecondaryLight(light)) {
                blinkHue = getSecondaryColor(mBlinkHues);
            } else if (isRedLight(light)) {
                blinkHue = Const.RED_COLOR;
            } else {
                // Skip unselected light
                continue;
            }

            final boolean isAvailable = saveOriginalHueIfAvailable(light);
            if (!isAvailable) {
                continue;
            }

            final float xy[] = PHUtilities.calculateXY(blinkHue, light.getModelNumber());
            final PHLightState lightState = new PHLightState();
            lightState.setOn(true);
            lightState.setColorMode(PHLight.PHLightColorMode.COLORMODE_XY);
            lightState.setX(xy[0]);
            lightState.setY(xy[1]);
            // For transition time, 1 = 100ms. Zero is supported, but not reliable
            lightState.setTransitionTime(HUE_FAST_TRANSITION / 100);

            final String validState = lightState.validateState();
            if (!TextUtils.isEmpty(validState)) {
                LOGW(TAG, "Invalid state for light #" + light.getUniqueId());
                bridge.updateLightState(light, blinkState); // Fallback: blink without changing colors
                continue;
            }

            bridge.updateLightState(light, lightState, new PHLightListener() {

                @Override
                public void onReceivingLightDetails(PHLight phLight) {
                }

                @Override
                public void onReceivingLights(List<PHBridgeResource> list) {
                }

                @Override
                public void onSearchComplete() {
                }

                @Override
                public void onSuccess() {
                    bridge.updateLightState(light, blinkState);
                }

                @Override
                public void onError(int i, String s) {

                }

                @Override
                public void onStateUpdate(Map<String, String> map, List<PHHueError> list) {
                }
            });
        }

        restoreLightsAfterDelay(isOpponent ? Const.BLINK_SHORT_DURATION : blinkDuration);
    }

    private void restoreLightsAfterDelay(int seconds) {
        final HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        new Handler(handlerThread.getLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                restoreLight();
            }
        }, seconds * DateUtils.SECOND_IN_MILLIS);
    }

    private void restoreLight() {
        final PHBridge bridge = hueSDK.getSelectedBridge();
        boolean hadLightsOff = false;
        for (final PHLight light : bridge.getResourceCache().getAllLights()) {
            if (!isLightSelected(light)) {
                continue;
            }

            final PHLightState previousState = previousStates.get(light.getUniqueId());
            final PHLightState lightState = HueBridgeUtils.cloneLightState(previousState);
            if (lightState != null) {
                hadLightsOff = !previousState.isOn() || hadLightsOff;
                lightState.setAlertMode(PHLight.PHLightAlertMode.ALERT_NONE);
                lightState.setOn(true);
                // For transition time, 4 = 400ms
                lightState.setTransitionTime(HUE_DEFAULT_TRANSITION / 100);

                final String validState = lightState.validateState();
                if (!TextUtils.isEmpty(validState)) {
                    LOGW(TAG, "Invalid state for light #" + light.getUniqueId());
                    continue;
                }
                bridge.updateLightState(light, lightState);
            }
        }

        if (hadLightsOff) {
            // We must wait for the color transition (400ms) before switching off
            switchOffLightsAfterDelay();
        } else {
            bus.post(new SyncBusEvents.LightBlinkStopped());
        }
    }

    private void switchOffLightsAfterDelay() {
        final HandlerThread handlerThread = new HandlerThread(TAG);
        handlerThread.start();
        new Handler(handlerThread.getLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                switchOffLights();
            }
        }, DELAY_SWITCH_OFF);
    }

    private void switchOffLights() {
        final PHBridge bridge = hueSDK.getSelectedBridge();
        final PHLightState offState = new PHLightState();
        offState.setOn(false);

        for (final PHLight light : bridge.getResourceCache().getAllLights()) {
            if (!isLightSelected(light)) {
                continue;
            }

            final PHLightState previousState = previousStates.get(light.getUniqueId());
            if (previousState != null && !previousState.isOn()) {
                bridge.updateLightState(light, offState);
            }
        }

        bus.post(new SyncBusEvents.LightBlinkStopped());
    }

    private boolean isPrimaryLight(PHLight light) {
        return primaryLights.contains(light.getUniqueId());
    }

    private boolean isSecondaryLight(PHLight light) {
        return secondaryLights.contains(light.getUniqueId());
    }

    private boolean isRedLight(PHLight light) {
        return redLights.contains(light.getUniqueId());
    }

    private boolean isLightSelected(PHLight light) {
        return isPrimaryLight(light) || isSecondaryLight(light) || isRedLight(light);
    }

    /**
     * Save light's original colors
     *
     * @param light
     * @return true if saved. false if unreachable or already blinking
     */
    private boolean saveOriginalHueIfAvailable(@NonNull PHLight light) {
        final PHLightState state = light.getLastKnownLightState();
        if (state == null) {
            return false;
        }

        final PHLight.PHLightAlertMode alertMode = state.getAlertMode();
        /*
         * When triggered manually, we don't check if reachable or already blinking.
         * This also helps fix non-responsive lights that were stuck in blinking AlertMode.
         */
        if (!isManualBlink && (!state.isReachable() ||
                HueBridgeUtils.areEqualModes(alertMode, PHLight.PHLightAlertMode.ALERT_LSELECT) ||
                HueBridgeUtils.areEqualModes(alertMode, PHLight.PHLightAlertMode.ALERT_SELECT))) {
            LOGW(TAG, "Light " + light.getUniqueId() + " is not available. Unreachable or already blinking?");

            // FIXME Issue related to Hue Bridge, where reachable lights report as not. Disabled for now
            // return false;
        }
        previousStates.put(light.getUniqueId(), HueBridgeUtils.cloneLightState(state));

        return true;
    }

    @ColorInt
    private static int getPrimaryColor(@NonNull List<Integer> blinkHues) {
        return blinkHues.get(0);
    }

    @ColorInt
    private static int getSecondaryColor(@NonNull List<Integer> blinkHues) {
        if (blinkHues.size() >= 2) {
            return blinkHues.get(1);
        }

        return ALTERNATE_RGB_COLOR;
    }
}
