/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.support.annotation.Nullable;
import android.support.v7.graphics.Palette;

import java.util.Collections;
import java.util.Comparator;

public class PaletteUtils {

    public static Palette.Swatch getLightSwatch(@Nullable Palette palette) {
        if (palette == null) {
            return null;
        }

        Palette.Swatch swatch = palette.getLightVibrantSwatch();
        if (swatch == null) {
            swatch = palette.getLightMutedSwatch();
        }

        return swatch;
    }

    public static Palette.Swatch getDominantSwatch(@Nullable Palette palette) {
        if (palette == null) {
            return null;
        }

        // find most-represented swatch based on population
        return Collections.max(palette.getSwatches(), new Comparator<Palette.Swatch>() {
            @Override
            public int compare(Palette.Swatch sw1, Palette.Swatch sw2) {
                final int diff = sw1.getPopulation() - sw2.getPopulation();
                return diff < 0 ? -1 : (diff == 0 ? 0 : 1);
            }
        });
    }
}
