/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class StringUtils {
    private static final String TAG = makeLogTag("StringUtils");

    private static final String CSV_SEPARATOR = ",";

    public static String joinCsv(Set<String> set) {
        return TextUtils.join(CSV_SEPARATOR, set);
    }

    public static String joinCsv(List<String> list) {
        return TextUtils.join(CSV_SEPARATOR, list);
    }

    public static String joinEscapeCsv(List<String> list) {
        return escapeCsv(TextUtils.join(CSV_SEPARATOR, list));
    }

    public static List<String> splitCsv(String csv) {
        return split(csv, CSV_SEPARATOR);
    }

    public static List<String> split(String joined, String delim) {
        return new ArrayList<>(
                Arrays.asList(TextUtils.split(joined, delim))
        );
    }

    public static String appendCsv(String csv, String value) {
        return csv + CSV_SEPARATOR + value;
    }

    /**
     * Escapes strings inside CSV
     *
     * @param csv
     * @return `"MTL","BOS"` for `MTL,BOS`
     */
    public static String escapeCsv(String csv) {
        if (!TextUtils.isEmpty(csv)) {
            return '"' + csv.replace(",", "\",\"") + '"';
        }

        return csv;
    }
}
