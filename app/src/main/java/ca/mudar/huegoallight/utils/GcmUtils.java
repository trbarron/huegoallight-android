/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.gcm.GcmPubSub;

import java.io.IOException;

import ca.mudar.huegoallight.Const;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class GcmUtils {
    private static final String TAG = makeLogTag("GcmUtils");

    private static final String PATH_TOPICS = "/topics/%s";

    public static void subscribeDefaultTopics(Context context, String token) throws IOException {
        final GcmPubSub pubSub = GcmPubSub.getInstance(context);
        for (String topic : Const.GCM_DEFAULT_TOPICS) {
            pubSub.subscribe(token, String.format(PATH_TOPICS, topic), null);
        }
    }

    public static void subscribeTeam(Context context, String token, String team) throws IOException {
        GcmPubSub.getInstance(context)
                .subscribe(token, String.format(PATH_TOPICS, team), null);
    }

    public static void unsubscribeTeam(Context context, String token, String team) throws IOException {
        GcmPubSub.getInstance(context)
                .unsubscribe(token, String.format(PATH_TOPICS, team));
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    public static boolean checkPlayServices(final Activity activity) {
        final GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        final int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.showErrorDialogFragment(
                        activity,
                        resultCode,
                        Const.RequestCodes.PLAY_SERVICES_RESOLUTION_REQUEST,
                        new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialog) {
                                activity.finish();
                            }
                        });
            }

            return false;
        }
        return true;
    }

}
