/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.provider;

import android.net.Uri;
import android.provider.BaseColumns;

import java.util.List;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.utils.StringUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class EventsContract {
    private static final String TAG = makeLogTag("EventsContract");

    public static final String CONTENT_AUTHORITY = "ca.mudar.huegoallight.provider";
    private static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    private static final String PATH_GAMES = "games";
    private static final String PATH_CURRENT = "current";
    private static final String PATH_UPCOMING = "upcoming";

    interface GamesColumns {
        String REMOTE_ID = "remote_id";
        String STARTS_AT = "starts_at";
        String AWAY_TEAM_SLUG = "away_team_slug";
        String HOME_TEAM_SLUG = "home_team_slug";
        String AWAY_TEAM_SCORE = "away_team_score";
        String HOME_TEAM_SCORE = "home_team_score";
        String DELAY = "delay";
        String IS_MUTED = "is_muted";
        String UPDATED_AT = "updated_at";
        String URL = "url";
    }

    public static class Games implements GamesColumns, BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon()
                .appendPath(PATH_GAMES).build();

        public static final Uri CONTENT_URI_CURRENT = CONTENT_URI.buildUpon()
                .appendPath(PATH_CURRENT).build();

        public static final Uri CONTENT_URI_UPCOMING = CONTENT_URI.buildUpon()
                .appendPath(PATH_UPCOMING).build();

        public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.huegoallight.game";
        public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.huegoallight.game";

        public static final String DEFAULT_SORT = STARTS_AT + " ASC ";
        public static final String RANDOM_SORT = " RANDOM() ";

        public static Uri buildGameUri(String id) {
            return CONTENT_URI.buildUpon().appendPath(id).build();
        }

        public static String getGameId(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        /**
         * Get cursor team selection, querying both away and home columns for provided slugs
         *
         * @param teams
         * @return selection filter, or empty query if not following any teams
         */
        public static String getSelectionByTeams(List<String> teams) {
            return getSelectionByTeams(teams, false);
        }

        /**
         * Get cursor team selection, querying both away and home columns for provided slugs
         *
         * @param teams
         * @param allowEmptySelection if true, forces empty query (_ID = -1). Used only when teams array is empty
         * @return selection filter
         */
        public static String getSelectionByTeams(List<String> teams, boolean allowEmptySelection) {
            if (teams == null || teams.size() == 0) {
                /**
                 * A null selection (no filter) would return all games,
                 * so unless allowEmptySelection is true, force an empty query (_ID=-1)
                 */
                return allowEmptySelection ? null :
                        Games._ID + "=" + Const.UNKNOWN_VALUE;
            }

            final String teamsSelection = StringUtils.joinEscapeCsv(teams);

            return Games.AWAY_TEAM_SLUG + " IN (" + teamsSelection + ") OR "
                    + Games.HOME_TEAM_SLUG + " IN (" + teamsSelection + ")";
        }
    }
}
