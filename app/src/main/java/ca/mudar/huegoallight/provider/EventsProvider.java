/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.utils.SelectionBuilder;

import static ca.mudar.huegoallight.provider.EventsContract.Games;
import static ca.mudar.huegoallight.provider.EventsDatabase.Tables;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class EventsProvider extends ContentProvider {
    private static final String TAG = makeLogTag("EventsProvider");

    private EventsDatabase mOpenHelper;

    private static final UriMatcher URI_MATCHER = buildUriMatcher();

    private static final int GAMES = 100;
    private static final int GAMES_CURRENT = 101;
    private static final int GAMES_UPCOMING = 102;
    private static final int GAMES_ID = 103;

    private static UriMatcher buildUriMatcher() {
        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = EventsContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, "games", GAMES);
        matcher.addURI(authority, "games/current", GAMES_CURRENT);
        matcher.addURI(authority, "games/upcoming", GAMES_UPCOMING);
        matcher.addURI(authority, "games/*", GAMES_ID);

        return matcher;
    }

    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mOpenHelper = new EventsDatabase(context);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final SQLiteDatabase db = mOpenHelper.getReadableDatabase();

        final int match = URI_MATCHER.match(uri);
        final SelectionBuilder builder = buildExpandedSelection(uri, match);

        Cursor c = builder.where(selection, selectionArgs).query(db, projection, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);

        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case GAMES_UPCOMING:
            case GAMES_CURRENT:
            case GAMES:
                return Games.CONTENT_TYPE;
            case GAMES_ID:
                return Games.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = URI_MATCHER.match(uri);

        switch (match) {
            case GAMES: {
                db.insertWithOnConflict(Tables.GAMES, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                getContext().getContentResolver().notifyChange(uri, null);
                return Games.buildGameUri(values.getAsString(Games.REMOTE_ID));
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).delete(db);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final SelectionBuilder builder = buildSimpleSelection(uri);
        int retVal = builder.where(selection, selectionArgs).update(db, values);
        getContext().getContentResolver().notifyChange(uri, null);
        return retVal;
    }

    private SelectionBuilder buildSimpleSelection(Uri uri) {
        final SelectionBuilder builder = new SelectionBuilder();
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case GAMES: {
                return builder.table(Tables.GAMES);
            }
            case GAMES_ID: {
                final String id = EventsContract.Games.getGameId(uri);
                return builder.table(EventsDatabase.Tables.GAMES).where(BaseColumns._ID + "=?", id);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    private SelectionBuilder buildExpandedSelection(Uri uri, int match) {
        final SelectionBuilder builder = new SelectionBuilder();
        switch (match) {
            case GAMES_CURRENT: {
                // 24 hours surrounding currentTime
                final long startOffset = System.currentTimeMillis() - 2 * Const.GAME_EXPIRY;
                final long endOffset = System.currentTimeMillis() + Const.GAME_EXPIRY;
                return builder.table(Tables.GAMES)
                        .where(Games.STARTS_AT + ">? ", String.valueOf(startOffset))
                        .where(Games.STARTS_AT + "<? ", String.valueOf(endOffset));
            }
            case GAMES_UPCOMING: {
                // Add 12 hours, to avoid re-displaying current game
                final long startOffset = System.currentTimeMillis() + Const.GAME_EXPIRY;
                return builder.table(Tables.GAMES).where(Games.STARTS_AT + ">?", String.valueOf(startOffset));
            }
            case GAMES_ID: {
                final String id = EventsContract.Games.getGameId(uri);
                return builder.table(EventsDatabase.Tables.GAMES).where(Games.REMOTE_ID + "=?", id);
            }
            default: {
                throw new UnsupportedOperationException("Unknown uri: " + uri);
            }
        }
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations)
            throws OperationApplicationException {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();

        db.beginTransaction();
        try {
            final int numOperations = operations.size();
            final ContentProviderResult[] results = new ContentProviderResult[numOperations];
            for (int i = 0; i < numOperations; i++) {
                results[i] = operations.get(i).apply(this, results, i);
            }

            db.setTransactionSuccessful();
//            notifyChanges(results);
            getContext().getContentResolver().notifyChange(Games.CONTENT_URI, null);
            return results;
        } finally {
            db.endTransaction();
        }
    }

    /**
     * Notify unique URIs when applying batch operations
     *
     * @param results
     */
    private void notifyChanges(ContentProviderResult[] results) {
        Set<Uri> foundUris = new HashSet<>();
        if (results != null) {
            for (ContentProviderResult result : results) {
                if (result.uri != null) {
                    if (!foundUris.contains(result.uri)) {
                        foundUris.add(result.uri);
                    }
                }
            }
        }

        for (Uri uri : foundUris) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }
}
