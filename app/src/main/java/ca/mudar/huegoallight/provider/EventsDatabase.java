/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import ca.mudar.huegoallight.Const;

import static ca.mudar.huegoallight.provider.EventsContract.GamesColumns;
import static ca.mudar.huegoallight.utils.LogUtils.LOGD;
import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class EventsDatabase extends SQLiteOpenHelper {
    private static final String TAG = makeLogTag("EventsDatabase");
    private static final int MUTE_DELAY_VERSION = 6;

    public EventsDatabase(Context context) {
        super(context, Const.DATABASE_NAME, null, Const.DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + Tables.GAMES + " (" +
                BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                GamesColumns.REMOTE_ID + " TEXT NOT NULL," +
                GamesColumns.STARTS_AT + " INTEGER NOT NULL," +
                GamesColumns.AWAY_TEAM_SLUG + " TEXT NOT NULL," +
                GamesColumns.HOME_TEAM_SLUG + " TEXT NOT NULL," +
                GamesColumns.AWAY_TEAM_SCORE + " INTEGER NOT NULL DEFAULT '0'," +
                GamesColumns.HOME_TEAM_SCORE + " INTEGER NOT NULL DEFAULT '0'," +
                GamesColumns.DELAY + " INTEGER NOT NULL DEFAULT '0'," +
                GamesColumns.IS_MUTED + " INTEGER NOT NULL DEFAULT '0'," +
                GamesColumns.UPDATED_AT + " INTEGER NOT NULL," +
                GamesColumns.URL + " TEXT NULL )"
        );

        db.execSQL("CREATE UNIQUE INDEX " + Indexes.GAME_ID
                + " ON " + Tables.GAMES
                + " (" + GamesColumns.REMOTE_ID + ")");
        db.execSQL("CREATE INDEX " + Indexes.GAME_AWAY
                + " ON " + Tables.GAMES
                + " (" + GamesColumns.AWAY_TEAM_SLUG + ")");
        db.execSQL("CREATE INDEX " + Indexes.GAME_HOME
                + " ON " + Tables.GAMES
                + " (" + GamesColumns.HOME_TEAM_SLUG + ")");
        db.execSQL("CREATE INDEX " + Indexes.GAME_DATE
                + " ON " + Tables.GAMES
                + " (" + GamesColumns.STARTS_AT + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        LOGD(TAG, "onUpgrade() from " + oldVersion + " to " + newVersion);

        if (newVersion == MUTE_DELAY_VERSION) {
            onUpgradeDelayIsMuted(db);
        } else {
            onUpgradeDropCreate(db);
        }
    }

    /**
     * Add two fields `delay` and `is_muted`
     *
     * @param db
     */
    private void onUpgradeDelayIsMuted(SQLiteDatabase db) {
        db.execSQL("ALTER TABLE " + Tables.GAMES
                + " ADD " + GamesColumns.DELAY + " INTEGER NOT NULL DEFAULT '0'");
        db.execSQL("ALTER TABLE " + Tables.GAMES
                + " ADD " + GamesColumns.IS_MUTED + " INTEGER NOT NULL DEFAULT '0'");
    }

    /**
     * Default upgrade, can leave an empty database. See {@link Const.PrefsNames#SEASON_UPDATED_AT}
     *
     * @param db
     */
    private void onUpgradeDropCreate(SQLiteDatabase db) {
        // Start by dropping indexes
        db.execSQL("DROP INDEX IF EXISTS " + Indexes.GAME_AWAY);
        db.execSQL("DROP INDEX IF EXISTS " + Indexes.GAME_AWAY);
        db.execSQL("DROP INDEX IF EXISTS " + Indexes.GAME_HOME);
        db.execSQL("DROP INDEX IF EXISTS " + Indexes.GAME_DATE);

        // Follow by dropping tables
        db.execSQL("DROP TABLE IF EXISTS " + Tables.GAMES);

        onCreate(db);
    }

    public interface Tables {
        String GAMES = "games";
    }

    private interface Indexes {
        String GAME_ID = "game_id";
        String GAME_AWAY = "game_away";
        String GAME_HOME = "game_home";
        String GAME_DATE = "game_date";
    }
}
