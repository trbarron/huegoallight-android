/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.Const.PrefsValues;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.utils.HueBridgeUtils;
import ca.mudar.huegoallight.utils.LogUtils;
import ca.mudar.huegoallight.utils.StringUtils;
import ca.mudar.huegoallight.utils.TimeUtils;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class HuePrefs implements Const.PrefsNames {
    private static final String TAG = makeLogTag("HuePrefs");

    private static HuePrefs instance;
    private SharedPreferences mPrefs;
    private SharedPreferences.Editor mPrefsEditor;

    private HuePrefs(Context context) {
        mPrefs = context.getSharedPreferences(Const.APP_PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static HuePrefs getInstance(Context context) {
        if (instance == null) {
            instance = new HuePrefs(context);
        }
        return instance;
    }

    private SharedPreferences.Editor edit() {
        if (mPrefsEditor == null) {
            mPrefsEditor = mPrefs.edit();
        }

        return mPrefsEditor;
    }

    public static void setDefaults(Context context) {
        PreferenceManager.setDefaultValues(context,
                Const.APP_PREFS_NAME,
                Context.MODE_PRIVATE,
                R.xml.prefs_defaults,
                false);
    }

    public boolean isOnboarding() {
        return mPrefs.getBoolean(IS_ONBOARDING, true);
    }

    public void setOnboardingCompleted() {
        edit().putBoolean(IS_ONBOARDING, false)
                .commit();
    }

    public boolean isLightsBoardDiscovery() {
        return mPrefs.getBoolean(IS_LIGHTS_DISCOVERY, true);
    }

    public void setLightsBoardDiscoveryCompleted() {
        edit().putBoolean(IS_LIGHTS_DISCOVERY, false)
                .commit();
    }

    public long getScheduleLastUpdatedAt() {
        return mPrefs.getLong(SEASON_UPDATED_AT, 0);
    }

    public void setScheduleLastUpdatedAt(long timestamp) {
        edit().putLong(SEASON_UPDATED_AT, timestamp)
                .apply();
    }

    public String getGcmToken() {
        return mPrefs.getString(GCM_TOKEN, null);
    }

    public void setGcmToken(String token) {
        edit().putString(GCM_TOKEN, token)
                .apply();
    }

    public String getUsername() {
        return mPrefs.getString(LAST_USERNAME, null);
    }

    public void setUsername(String username) {
        edit().putString(LAST_USERNAME, username)
                .apply();
    }

    public String getIPAddress() {
        return mPrefs.getString(LAST_IP, null);
    }

    public void setIPAddress(String ipAddress) {
        edit().putString(LAST_IP, ipAddress)
                .apply();
    }

    public String getBridgeId() {
        return mPrefs.getString(LAST_BRIDGE_ID, null);
    }

    public void setBridgeId(String bridgeId) {
        edit().putString(LAST_BRIDGE_ID, bridgeId)
                .apply();
    }

    public void setBridgeConfiguration(String username, String ipAddress, String bridgeId) {
        edit().putString(LAST_USERNAME, username)
                .putString(LAST_IP, ipAddress)
                .putString(LAST_BRIDGE_ID, bridgeId)
                .apply();
    }

    public boolean isKnownBridge(String normalizedBridgeId) {
        return mPrefs.contains(String.format(BRIDGE_PRIMARY_LIGHTS, normalizedBridgeId)) ||
                mPrefs.contains(String.format(BRIDGE_SECONDARY_LIGHTS, normalizedBridgeId)) ||
                mPrefs.contains(String.format(BRIDGE_RED_LIGHTS, normalizedBridgeId));
    }

    public void setDefaultTeam(String team) {
        final String key = Const.PrefsNames.NHL_TEAM_PREFIX +
                team.toLowerCase(Locale.getDefault());

        edit().putBoolean(key, true)
                .putString(NHL_FOLLOWED_TEAMS, team)
                .apply();
    }

    public void followNhlTeam(String team) {
        final String csv = mPrefs.getString(NHL_FOLLOWED_TEAMS, null);

        if (TextUtils.isEmpty(csv)) {
            edit().putString(NHL_FOLLOWED_TEAMS, team)
                    .commit();
        } else if (!csv.contains(team)) {
            edit().putString(NHL_FOLLOWED_TEAMS, StringUtils.appendCsv(csv, team))
                    .commit();
        }
    }

    public void unfollowNhlTeam(String team) {
        final String csv = mPrefs.getString(NHL_FOLLOWED_TEAMS, null);

        if (!TextUtils.isEmpty(csv)) {
            final List<String> teams = StringUtils.splitCsv(csv);
            if (teams.remove(team)) {
                edit().putString(NHL_FOLLOWED_TEAMS, StringUtils.joinCsv(teams))
                        .commit();
            }
        }
    }

    public List<String> getFollowedNhlTeams() {
        final String csv = mPrefs.getString(NHL_FOLLOWED_TEAMS, null);

        if (!TextUtils.isEmpty(csv)) {
            return StringUtils.splitCsv(csv);
        }

        return new ArrayList<>();
    }

    public int getBlinkDuration() {
        final String duration = mPrefs.getString(BLINK_DURATION, PrefsValues.DEFAULT_BLINK_DURATION);
        try {
            return Integer.valueOf(duration);
        } catch (NumberFormatException e) {
            LogUtils.REMOTE_LOG(e);
        }

        return Integer.valueOf(PrefsValues.DEFAULT_BLINK_DURATION);
    }

    public boolean isFollowingTeam(String team) {
        if (TextUtils.isEmpty(team)) {
            return false;
        }

        final String csv = mPrefs.getString(NHL_FOLLOWED_TEAMS, null);
        if (!TextUtils.isEmpty(csv) && csv.contains(team)) {
            return true;
        }

        return false;
    }

    /**
     * Should ignore team, for a silent notification and no Hue blink
     *
     * @param team
     * @return when ignoring opponents' goals and not following this team
     */
    public boolean ignoreOpponentTeam(String team) {
        if (mPrefs.getBoolean(IGNORE_OPPONENT, true)) {
            return !isFollowingTeam(team);
        }

        return false;
    }

    public boolean hasNotifications() {
        return mPrefs.getBoolean(HAS_NOTIFICATIONS, false);
    }

    public Uri getRingtonePath() {
        try {
            return Uri.parse(mPrefs.getString(RINGTONE, null));
        } catch (NullPointerException e) {
            return null;
        }
    }

    public boolean hasVibration() {
        return mPrefs.getBoolean(HAS_VIBRATION, true);
    }

    public void setSelectedLights(@NonNull List<String> primaryLights) {
        setSelectedLights(primaryLights, new ArrayList<String>(), new ArrayList<String>());
    }

    public void setSelectedLights(@NonNull List<String> primaryLights,
                                  @NonNull List<String> secondaryLights,
                                  @NonNull List<String> redLights) {
        edit().putStringSet(PRIMARY_LIGHTS, new HashSet<>(primaryLights))
                .putStringSet(SECONDARY_LIGHTS, new HashSet<>(secondaryLights))
                .putStringSet(RED_LIGHTS, new HashSet<>(redLights))
                .apply();
    }

    public void saveLastBridgeSelectedLights() {
        final String normalizedBridgeId = HueBridgeUtils.getNormalizedBridgeId(mPrefs.getString(LAST_BRIDGE_ID, null));

        final String primaryKey = String.format(BRIDGE_PRIMARY_LIGHTS, normalizedBridgeId);
        final String secondaryKey = String.format(BRIDGE_SECONDARY_LIGHTS, normalizedBridgeId);
        final String redKey = String.format(BRIDGE_RED_LIGHTS, normalizedBridgeId);
        final Set<String> primaryValues = mPrefs.getStringSet(PRIMARY_LIGHTS, new HashSet<String>());
        final Set<String> secondaryValues = mPrefs.getStringSet(SECONDARY_LIGHTS, new HashSet<String>());
        final Set<String> redValues = mPrefs.getStringSet(RED_LIGHTS, new HashSet<String>());

        edit().putStringSet(primaryKey, primaryValues)
                .putStringSet(secondaryKey, secondaryValues)
                .putStringSet(redKey, redValues)
                .apply();
    }

    public void restoreBridgeSelectedLights(String normalizedBridgeId) {
        final String primaryKey = String.format(BRIDGE_PRIMARY_LIGHTS, normalizedBridgeId);
        final String secondaryKey = String.format(BRIDGE_SECONDARY_LIGHTS, normalizedBridgeId);
        final String redKey = String.format(BRIDGE_RED_LIGHTS, normalizedBridgeId);
        final Set<String> primaryValues = mPrefs.getStringSet(primaryKey, new HashSet<String>());
        final Set<String> secondaryValues = mPrefs.getStringSet(secondaryKey, new HashSet<String>());
        final Set<String> redValues = mPrefs.getStringSet(redKey, new HashSet<String>());

        edit().putStringSet(PRIMARY_LIGHTS, primaryValues)
                .putStringSet(SECONDARY_LIGHTS, secondaryValues)
                .putStringSet(RED_LIGHTS, redValues)
                .apply();
    }

    @NonNull
    public List<String> getPrimaryLights() {
        return getSelectedLights(PRIMARY_LIGHTS);
    }

    @NonNull
    public List<String> getSecondaryLights() {
        return getSelectedLights(SECONDARY_LIGHTS);
    }

    @NonNull
    public List<String> getRedLights() {
        return getSelectedLights(RED_LIGHTS);
    }

    @NonNull
    private List<String> getSelectedLights(String type) {
        final Set<String> lights = mPrefs.getStringSet(type, null);

        final ArrayList<String> list = new ArrayList<>();
        if (lights != null) {
            list.addAll(lights);
        }

        return list;
    }

    @NonNull
    public List<String> getAllSelectedLights() {
        final List<String> selected = getPrimaryLights();
        selected.addAll(getSecondaryLights());
        selected.addAll(getRedLights());

        return selected;
    }

    public boolean hasSelectedLights() {
        return (getPrimaryLights().size() > 0) || (getSecondaryLights().size() > 0) ||
                (getRedLights().size() > 0);
    }

    public boolean hasQuietHours() {
        return mPrefs.getBoolean(QUIET_HOURS_ENABLED, PrefsValues.DEFAULT_QUIET_HOURS_ENABLED);
    }

    public Calendar getQuietHoursStart() {
        return TimeUtils.getCalendarInstance(mPrefs.getInt(QUIET_HOURS_START, PrefsValues.DEFAULT_QUIET_HOURS_START));
    }

    public void setQuietHoursStart(int minutesOfDay) {
        edit().putInt(QUIET_HOURS_START, minutesOfDay)
                .apply();
    }

    public Calendar getQuietHoursEnd() {
        return TimeUtils.getCalendarInstance(mPrefs.getInt(QUIET_HOURS_END, PrefsValues.DEFAULT_QUIET_HOURS_END));
    }

    public void setQuietHoursEnd(int minutesOfDay) {
        edit().putInt(QUIET_HOURS_END, minutesOfDay)
                .apply();
    }

    public boolean isQuietHoursOvernight() {
        final int start = mPrefs.getInt(QUIET_HOURS_START, PrefsValues.DEFAULT_QUIET_HOURS_START);
        final int end = mPrefs.getInt(QUIET_HOURS_END, PrefsValues.DEFAULT_QUIET_HOURS_END);

        return start >= end;
    }

    public boolean isCurrentlyQuietHours() {
        return hasQuietHours() &&
                TimeUtils.isCurrentlyWithinPeriod(getQuietHoursStart(), getQuietHoursEnd());
    }
}
