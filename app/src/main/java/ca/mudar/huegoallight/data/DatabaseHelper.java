/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.data;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;

import ca.mudar.huegoallight.model.GameSettings;
import ca.mudar.huegoallight.model.NhlGame;
import ca.mudar.huegoallight.provider.EventsContract;
import ca.mudar.huegoallight.utils.Lists;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class DatabaseHelper {
    private static final String TAG = makeLogTag("DatabaseHelper");

    public static void addGamesToSchedule(Context context, List<NhlGame> games) {
        if (context == null) {
            return;
        }

        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        for (NhlGame game : games) {
            batch.add(game.getContentProviderInsertBuilder().build());
        }

        try {
            context.getContentResolver().applyBatch(EventsContract.CONTENT_AUTHORITY, batch);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    public static void updateGamesScores(Context context, List<NhlGame> games) {
        if (context == null) {
            return;
        }

        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        for (NhlGame game : games) {
            batch.add(game.getContentProviderUpdateBuilder().build());
        }

        try {
            context.getContentResolver().applyBatch(EventsContract.CONTENT_AUTHORITY, batch);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    public static void updateGameDelay(Context context, String gameId, int seconds) {
        if (context == null || TextUtils.isEmpty(gameId)) {
            return;
        }

        final ContentProviderOperation.Builder builder =
                ContentProviderOperation.newUpdate(EventsContract.Games.CONTENT_URI);
        builder.withSelection(EventsContract.Games.REMOTE_ID + "=?", new String[]{gameId});
        builder.withValue(EventsContract.Games.DELAY, seconds);

        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        batch.add(builder.build());

        try {
            context.getContentResolver().applyBatch(EventsContract.CONTENT_AUTHORITY, batch);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    public static void updateGameMute(Context context, String gameId, boolean mute) {
        if (context == null || TextUtils.isEmpty(gameId)) {
            return;
        }

        final ContentProviderOperation.Builder builder =
                ContentProviderOperation.newUpdate(EventsContract.Games.CONTENT_URI);
        builder.withSelection(EventsContract.Games.REMOTE_ID + "=?", new String[]{gameId});
        builder.withValue(EventsContract.Games.IS_MUTED, mute ? 1 : 0);

        final ArrayList<ContentProviderOperation> batch = Lists.newArrayList();
        batch.add(builder.build());

        try {
            context.getContentResolver().applyBatch(EventsContract.CONTENT_AUTHORITY, batch);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    @NonNull
    public static GameSettings getGameSettings(Context context, String gameId) {
        final GameSettings gameSettings = new GameSettings();
        if (context == null) {
            return gameSettings;
        }

        final Cursor cursor = context.getContentResolver().query(EventsContract.Games
                .buildGameUri(gameId), Queries.GamesSettingsQuery.PROJECTION, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            gameSettings.setDelay(cursor.getInt(Queries.GamesSettingsQuery.DELAY));
            gameSettings.setMuted(cursor.getInt(Queries.GamesSettingsQuery.IS_MUTED) == 1);
        }

        return gameSettings;
    }
}
