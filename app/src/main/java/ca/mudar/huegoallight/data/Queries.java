/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.data;

import ca.mudar.huegoallight.provider.EventsContract;

public class Queries {

    public interface UpcomingGamesQuery {
        int _TOKEN = 10;

        String[] PROJECTION = new String[]{
                EventsContract.Games.REMOTE_ID,
                EventsContract.Games.STARTS_AT,
                EventsContract.Games.AWAY_TEAM_SLUG,
                EventsContract.Games.HOME_TEAM_SLUG
        };

        int REMOTE_ID = 0;
        int STARTS_AT = 1;
        int AWAY_TEAM_SLUG = 2;
        int HOME_TEAM_SLUG = 3;
    }

    public interface CurrentGamesQuery extends UpcomingGamesQuery {
        int _TOKEN = 20;

        String[] PROJECTION = new String[]{
                EventsContract.Games.REMOTE_ID,
                EventsContract.Games.STARTS_AT,
                EventsContract.Games.AWAY_TEAM_SLUG,
                EventsContract.Games.HOME_TEAM_SLUG,
                EventsContract.Games.AWAY_TEAM_SCORE,
                EventsContract.Games.HOME_TEAM_SCORE,
                EventsContract.Games.IS_MUTED,
                EventsContract.Games.DELAY,
                EventsContract.Games.URL
        };

        int AWAY_TEAM_SCORE = 4;
        int HOME_TEAM_SCORE = 5;
        int IS_MUTED = 6;
        int DELAY = 7;
        int URL = 8;
    }

    public interface GamesSettingsQuery {
        int _TOKEN = 10;

        String[] PROJECTION = new String[]{
                EventsContract.Games.REMOTE_ID,
                EventsContract.Games.IS_MUTED,
                EventsContract.Games.DELAY
        };

        int REMOTE_ID = 0;
        int IS_MUTED = 1;
        int DELAY = 2;
    }
}
