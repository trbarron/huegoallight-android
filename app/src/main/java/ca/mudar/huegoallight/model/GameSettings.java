/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.model;

public class GameSettings {
    private boolean isMuted;
    private int delay;

    public GameSettings() {
        this.isMuted = false;
        this.delay = 0;
    }

    public boolean isMuted() {
        return isMuted;
    }

    public void setMuted(boolean muted) {
        isMuted = muted;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public boolean hasDelay() {
        return delay > 0;
    }

    @Override
    public String toString() {
        return "GameSettings{" +
                "isMuted=" + isMuted +
                ", delay=" + delay +
                '}';
    }
}
