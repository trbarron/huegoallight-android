/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.model;

import android.content.ContentProviderOperation;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.io.model.Game;
import ca.mudar.huegoallight.io.model.GameAttributes;
import ca.mudar.huegoallight.io.model.Link;
import ca.mudar.huegoallight.provider.EventsContract;

public class NhlGame extends Game {
    
    public String getGameCenterUrl() {
        if (links != null) {
            final Link link = links.getRelated();
            if (link != null && Const.ApiValues.GAMECENTER_LINK.equals(link.getMeta())) {
                return link.getHref();
            }
        }

        return null;
    }

    public ContentProviderOperation.Builder getContentProviderInsertBuilder() {
        final ContentProviderOperation.Builder builder =
                ContentProviderOperation.newInsert(EventsContract.Games.CONTENT_URI);

        final long now = System.currentTimeMillis();

        builder.withValue(EventsContract.Games.REMOTE_ID, getId());
        builder.withValue(EventsContract.Games.URL, getGameCenterUrl());
        final GameAttributes attrs = getAttributes();
        if (attrs != null) {
            builder.withValue(EventsContract.Games.STARTS_AT, attrs.getStartsAt());
            if (attrs.getAway() != null) {
                builder.withValue(EventsContract.Games.AWAY_TEAM_SLUG, attrs.getAway().getSlug());
                builder.withValue(EventsContract.Games.AWAY_TEAM_SCORE, attrs.getAway().getGoals());
            }
            if (attrs.getHome() != null) {
                builder.withValue(EventsContract.Games.HOME_TEAM_SLUG, attrs.getHome().getSlug());
                builder.withValue(EventsContract.Games.HOME_TEAM_SCORE, attrs.getHome().getGoals());
            }
            builder.withValue(EventsContract.Games.UPDATED_AT, now);
        }

        return builder;
    }

    public ContentProviderOperation.Builder getContentProviderUpdateBuilder() {
        final ContentProviderOperation.Builder builder =
                ContentProviderOperation.newUpdate(EventsContract.Games.CONTENT_URI);

        final long now = System.currentTimeMillis();

        builder.withSelection(EventsContract.Games.REMOTE_ID + "=?", new String[]{getId()});
        builder.withValue(EventsContract.Games.REMOTE_ID, getId());

        final GameAttributes attrs = getAttributes();
        if (attrs != null) {
            builder.withValue(EventsContract.Games.STARTS_AT, attrs.getStartsAt());
            if (attrs.getAway() != null) {
                builder.withValue(EventsContract.Games.AWAY_TEAM_SCORE, attrs.getAway().getGoals());
            }
            if (attrs.getHome() != null) {
                builder.withValue(EventsContract.Games.HOME_TEAM_SCORE, attrs.getHome().getGoals());
            }
            builder.withValue(EventsContract.Games.UPDATED_AT, now);
        }

        return builder;
    }
}
