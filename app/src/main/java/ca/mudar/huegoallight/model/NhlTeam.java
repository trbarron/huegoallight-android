/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.model;

import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.R;
import ca.mudar.huegoallight.io.model.Team;
import ca.mudar.huegoallight.utils.HockeyUtils;

public class NhlTeam extends Team {
    @ColorInt
    private int color;

    public NhlTeam(@NonNull String slug) {
        this(slug, Const.DEFAULT_RGB_COLOR);
    }

    public NhlTeam(@NonNull String slug, @ColorInt int color) {
        super(slug);
        this.color = color;
    }

    @DrawableRes
    public int getTeamLogo() {
        switch (getSlug()) {
            case "ANA":
                return R.drawable.logo_ana;
            case "ARI":
                return R.drawable.logo_ari;
            case "BOS":
                return R.drawable.logo_bos;
            case "BUF":
                return R.drawable.logo_buf;
            case "CAR":
                return R.drawable.logo_car;
            case "CBJ":
                return R.drawable.logo_cbj;
            case "CGY":
                return R.drawable.logo_cgy;
            case "CHI":
                return R.drawable.logo_chi;
            case "COL":
                return R.drawable.logo_col;
            case "DAL":
                return R.drawable.logo_dal;
            case "DET":
                return R.drawable.logo_det;
            case "EDM":
                return R.drawable.logo_edm;
            case "FLA":
                return R.drawable.logo_fla;
            case "LAK":
                return R.drawable.logo_lak;
            case "MIN":
                return R.drawable.logo_min;
            case "MTL":
                return R.drawable.logo_mtl;
            case "NJD":
                return R.drawable.logo_njd;
            case "NSH":
                return R.drawable.logo_nsh;
            case "NYI":
                return R.drawable.logo_nyi;
            case "NYR":
                return R.drawable.logo_nyr;
            case "OTT":
                return R.drawable.logo_ott;
            case "PHI":
                return R.drawable.logo_phi;
            case "PIT":
                return R.drawable.logo_pit;
            case "SJS":
                return R.drawable.logo_sjs;
            case "STL":
                return R.drawable.logo_stl;
            case "TBL":
                return R.drawable.logo_tbl;
            case "TOR":
                return R.drawable.logo_tor;
            case "VAN":
                return R.drawable.logo_van;
            case "VGK":
                return R.drawable.logo_vgk;
            case "WPG":
                return R.drawable.logo_wpg;
            case "WSH":
                return R.drawable.logo_wsh;
        }

        return R.drawable.empty_drawable;
    }

    @StringRes
    public int getTeamName() {
        switch (getSlug()) {
            case "ANA":
                return R.string.nhl_team_name_ana;
            case "ARI":
                return R.string.nhl_team_name_ari;
            case "BOS":
                return R.string.nhl_team_name_bos;
            case "BUF":
                return R.string.nhl_team_name_buf;
            case "CAR":
                return R.string.nhl_team_name_car;
            case "CBJ":
                return R.string.nhl_team_name_cbj;
            case "CGY":
                return R.string.nhl_team_name_cgy;
            case "CHI":
                return R.string.nhl_team_name_chi;
            case "COL":
                return R.string.nhl_team_name_col;
            case "DAL":
                return R.string.nhl_team_name_dal;
            case "DET":
                return R.string.nhl_team_name_det;
            case "EDM":
                return R.string.nhl_team_name_edm;
            case "FLA":
                return R.string.nhl_team_name_fla;
            case "LAK":
                return R.string.nhl_team_name_lak;
            case "MIN":
                return R.string.nhl_team_name_min;
            case "MTL":
                return R.string.nhl_team_name_mtl;
            case "NJD":
                return R.string.nhl_team_name_njd;
            case "NSH":
                return R.string.nhl_team_name_nsh;
            case "NYI":
                return R.string.nhl_team_name_nyi;
            case "NYR":
                return R.string.nhl_team_name_nyr;
            case "OTT":
                return R.string.nhl_team_name_ott;
            case "PHI":
                return R.string.nhl_team_name_phi;
            case "PIT":
                return R.string.nhl_team_name_pit;
            case "SJS":
                return R.string.nhl_team_name_sjs;
            case "STL":
                return R.string.nhl_team_name_stl;
            case "TBL":
                return R.string.nhl_team_name_tbl;
            case "TOR":
                return R.string.nhl_team_name_tor;
            case "VAN":
                return R.string.nhl_team_name_van;
            case "VGK":
                return R.string.nhl_team_name_vgk;
            case "WPG":
                return R.string.nhl_team_name_wpg;
            case "WSH":
                return R.string.nhl_team_name_wsh;
        }

        return R.string.empty_string;
    }

    @StringRes
    public int getTeamShortName() {
        switch (getSlug()) {
            case "ANA":
                return R.string.nhl_team_short_name_ana;
            case "ARI":
                return R.string.nhl_team_short_name_ari;
            case "BOS":
                return R.string.nhl_team_short_name_bos;
            case "BUF":
                return R.string.nhl_team_short_name_buf;
            case "CAR":
                return R.string.nhl_team_short_name_car;
            case "CBJ":
                return R.string.nhl_team_short_name_cbj;
            case "CGY":
                return R.string.nhl_team_short_name_cgy;
            case "CHI":
                return R.string.nhl_team_short_name_chi;
            case "COL":
                return R.string.nhl_team_short_name_col;
            case "DAL":
                return R.string.nhl_team_short_name_dal;
            case "DET":
                return R.string.nhl_team_short_name_det;
            case "EDM":
                return R.string.nhl_team_short_name_edm;
            case "FLA":
                return R.string.nhl_team_short_name_fla;
            case "LAK":
                return R.string.nhl_team_short_name_lak;
            case "MIN":
                return R.string.nhl_team_short_name_min;
            case "MTL":
                return R.string.nhl_team_short_name_mtl;
            case "NJD":
                return R.string.nhl_team_short_name_njd;
            case "NSH":
                return R.string.nhl_team_short_name_nsh;
            case "NYI":
                return R.string.nhl_team_short_name_nyi;
            case "NYR":
                return R.string.nhl_team_short_name_nyr;
            case "OTT":
                return R.string.nhl_team_short_name_ott;
            case "PHI":
                return R.string.nhl_team_short_name_phi;
            case "PIT":
                return R.string.nhl_team_short_name_pit;
            case "SJS":
                return R.string.nhl_team_short_name_sjs;
            case "STL":
                return R.string.nhl_team_short_name_stl;
            case "TBL":
                return R.string.nhl_team_short_name_tbl;
            case "TOR":
                return R.string.nhl_team_short_name_tor;
            case "VAN":
                return R.string.nhl_team_short_name_van;
            case "VGK":
                return R.string.nhl_team_short_name_vgk;
            case "WPG":
                return R.string.nhl_team_short_name_wpg;
            case "WSH":
                return R.string.nhl_team_short_name_wsh;
        }

        return R.string.empty_string;
    }

    @ColorInt
    public int getColor() {
        return color;
    }

    @DrawableRes
    public int getTeamNotifyIcon() {
        return HockeyUtils.getTeamNotifyIcon(getSlug());
    }

}
