/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.io;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.io.model.NhlGamesList;
import ca.mudar.huegoallight.io.model.NhlSingleGame;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface NhlApiService {
    String CONTENT_TYPE = "Content-type: application/json";

    @Headers({CONTENT_TYPE})
    @GET(Const.ApiPaths.GET_SEASON_CLENDAR)
    Call<NhlGamesList> getSeasonCalendar(
            @Query(Const.ApiArgs.LAST_UPDATED_AT) long timestamp
    );

    @Headers({CONTENT_TYPE})
    @GET(Const.ApiPaths.GET_CLUB_CLENDAR)
    Call<NhlGamesList> getClubCalendar(
            @Path(Const.ApiArgs.CLUB) String club,
            @Query(Const.ApiArgs.LAST_UPDATED_AT) long timestamp
    );

    @Headers({CONTENT_TYPE})
    @GET(Const.ApiPaths.GET_MULTI_LIVE_GAMES)
    Call<NhlGamesList> getLiveGames(
            @Header(Const.ApiArgs.TOKEN) String token,
            @Path(Const.ApiArgs.GAMES) String games
    );

    @Headers({CONTENT_TYPE})
    @GET(Const.ApiPaths.GET_LIVE_GAME)
    Call<NhlSingleGame> getDefaultLive(
            @Header(Const.ApiArgs.TOKEN) String token
    );
}
