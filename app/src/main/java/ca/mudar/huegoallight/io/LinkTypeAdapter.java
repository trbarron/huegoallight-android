/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.io;

import android.text.TextUtils;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

import ca.mudar.huegoallight.Const;
import ca.mudar.huegoallight.io.model.Link;
import ca.mudar.huegoallight.utils.LogUtils;

public class LinkTypeAdapter extends TypeAdapter<Link> {

    @Override
    public Link read(JsonReader reader) throws IOException {
        Link link = new Link();
        try {
            if (reader.peek() == JsonToken.NULL) {
                reader.nextNull();
                return null;
            } else if (reader.peek().equals(JsonToken.STRING)) {
                // Link member is a plain string
                link.setHref(reader.nextString());
            } else if (reader.peek() == JsonToken.BEGIN_OBJECT) {
                // Link member is a Link object, containing `href` and `meta` string members
                reader.beginObject();
                while (!reader.peek().equals(JsonToken.END_OBJECT)) {
                    if (reader.peek().equals(JsonToken.NAME)) {
                        final String name = reader.nextName();
                        if (name.equals(Const.ApiValues.LINK_HREF)) {
                            // The link URI (string)
                            link.setHref(reader.nextString());
                        } else if (name.equals(Const.ApiValues.LINK_META)) {
                            // The link meta string
                            link.setMeta(reader.nextString());
                        } else {
                            reader.skipValue();
                        }
                    }
                }
                reader.endObject();
            }
        } catch (IOException e) {
            LogUtils.REMOTE_LOG(e);
        }

        // Return null if href is empty
        return !TextUtils.isEmpty(link.getHref()) ? link : null;
    }

    @Override
    public void write(JsonWriter writer, Link link) throws IOException {
        if (link == null || TextUtils.isEmpty(link.getHref())) {
            // Empty object
            writer.nullValue();
        } else if (TextUtils.isEmpty(link.getMeta())) {
            // Plain string URL
            writer.value(link.getHref());
        } else {
            // Link object, containing `href` and `meta` string members
            writer.name(link.getMeta())
                    .value(link.getHref());
        }
    }

}
