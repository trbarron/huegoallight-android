/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.io.model;

import android.text.format.DateUtils;

public class NhlGamesListMeta {
    private long timestamp;
    private boolean dataFiltered;

    /**
     * Returns lastUpdatedAt timestamp, in Unix seconds.
     * This is stored in prefs and used as a query param to filter results.
     * Use {@link #getTimestampMillis() getTimestampMillis} to use timestamp value in android app
     *
     * @return seconds
     */
    public long getTimestamp() {
        return timestamp;
    }

    /**
     * @return milliseconds
     * @see NhlGamesListMeta#getTimestamp()
     */
    public long getTimestampMillis() {
        return timestamp * DateUtils.SECOND_IN_MILLIS;
    }

    public boolean isDataFiltered() {
        return dataFiltered;
    }
}
