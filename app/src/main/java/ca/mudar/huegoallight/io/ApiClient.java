/*
    Hue Hockey Goal Light
    Make your Philips Hue lights blink in celebration of your
    NHL team’s goals.

    Copyright (C) 2016 Mudar Noufal <mn@mudar.ca>

    This file is part of HueGoalLight.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package ca.mudar.huegoallight.io;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import ca.mudar.huegoallight.BuildConfig;
import ca.mudar.huegoallight.io.model.Link;
import ca.mudar.huegoallight.io.model.NhlGamesList;
import ca.mudar.huegoallight.io.model.NhlSingleGame;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static ca.mudar.huegoallight.utils.LogUtils.makeLogTag;

public class ApiClient {
    private static final String TAG = makeLogTag("ApiClient");

    public static NhlApiService getService() {
        return getService(false);
    }

    @Deprecated
    public static NhlApiService getServiceLog() {
        return getService(true);
    }

    /**
     * Get the NHL API service
     *
     * @param httpLogging Enable verbose
     * @return NhlApiService
     */
    private static NhlApiService getService(boolean httpLogging) {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(httpLogging ? HttpLoggingInterceptor.Level.BODY :
                HttpLoggingInterceptor.Level.NONE);

        final OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(new HttpErrorInterceptor())
                .addInterceptor(interceptor)
                .build();

        final Gson gson = new GsonBuilder()
                .registerTypeAdapter(Link.class, new LinkTypeAdapter())
                .create();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(NhlApiService.class);
    }

    public static void getSeasonCalendar(NhlApiService service, long timestamp, Callback<NhlGamesList> cb) {
        service.getSeasonCalendar(timestamp)
                .enqueue(cb);
    }

    public static NhlGamesList getSeasonCalendar(NhlApiService service, long timestamp) {
        try {
            final Response<NhlGamesList> response = service.getSeasonCalendar(timestamp)
                    .execute();

            if (response.isSuccessful()) {
                return response.body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void getClubCalendar(NhlApiService service, String team, long timestamp, Callback<NhlGamesList> cb) {
        service.getClubCalendar(team, timestamp)
                .enqueue(cb);
    }

    public static void getLiveGames(NhlApiService service, String token, String games, Callback<NhlGamesList> cb) {
        service.getLiveGames(token, games)
                .enqueue(cb);
    }

    /**
     * Does not make much sense to rely on the API's default game
     *
     * @param service
     * @param cb
     */
    @Deprecated
    public static void getLiveGame(NhlApiService service, String token, Callback<NhlSingleGame> cb) {
        service.getDefaultLive(token)
                .enqueue(cb);
    }
}
