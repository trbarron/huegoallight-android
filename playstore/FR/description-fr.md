## Title
*30 chars*

Lumière de but Hue

## Short description
*80 chars* 

Faites clignoter vos lumières Philips Hue pour célébrer les buts de votre équipe

## Full description
*4000 chars*

IMPORTANT: Ne pas installer si vous n’avez pas un pont Philips Hue. L’application ne sert à rien sans ces lumières Hue.

Suivez votre équipe NHL préférée et vous serez notifiés par Philips Hue quand ils comptent un but…

Veuillez noter que les résultats ne sont pas en temps réel. Le délai habituel est autour de 30-60 secondes comparé à la télédiffusion en direct. Le délai est inférieur si vous regardez la partie en streaming, ou si vous pouvez utiliser la fonction pause-rebobinage pour votre diffusion.

Si l’app est plus rapide que votre diffusion de la partie, vos lumières peuvent clignoter avant que vous ne voyez le but à l’écran. Pour éviter de divulgâcher le résultat, vous pouvez retarder le clignotement pour une durée allant jusqu’à une minute.


Projet de logiciel libre, disponible sur GitLab. Le code source est publié sous la licence GPLv3 de la Free Software Foundation: https://gitlab.com/mudar-ca/huegoallight-android

NOTE JURIDIQUE
---
Cette application est un projet amateur qui n’est pas affilié, endossé ni commandité par la LNH, les équipes de la LNH ou Philips Lighting B.V.

NHL et l’emblème NHL sont des marques de commerce déposées, et LNH et l’emblème LNH sont des marques de commerce de la Ligue Nationale de Hockey.  Tous les logotypes et toutes les marques de la LNH, ainsi que les logotypes et les marques des équipes de la LNH illustrés aux présentes, appartiennent à la LNH et à ses équipes respectives et ne peuvent être reproduits sans le consentement préalable écrit de NHL Enterprises, L.P. © LNH. Tous droits réservés.

Philips et Philips Hue sont des marques de commerce déposées de Koninklijke Philips N.V. © Philips Lighting B.V. Tous droits réservés.
